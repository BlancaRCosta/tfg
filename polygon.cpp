#include "polygon.h"

///////////////////////////////////////

Polygon::Polygon()
{

}

///////////////////////////////////////

Polygon::Polygon(vector<Point> vertexes, QColor color, QString polygonName, int numPoly, bool instantiableState, int polygonID, QRectF BBox)
{
    polyVertexes=vertexes;
    polyColor=color;
    QString aux("_");

    polygonName.append(aux);
    polygonName.append(QString::number(numPoly));
    polyName=polygonName;

    instanStateP=instantiableState;

    polyID=polygonID;

    this->BBox=BBox;
 }

///////////////////////////////////////

Polygon::Polygon(const Polygon &Plg)
{
    polyVertexes=Plg.polyVertexes;
    polyColor=Plg.polyColor;
    polyName=Plg.polyName;
    instanStateP=Plg.instanStateP;
    polyID=Plg.polyID;
    BBox= Plg.BBox;
}

///////////////////////////////////////

Polygon::~Polygon()
{

}

///////////////////////////////////////

void Polygon::setPolyName(int index, QString polygonName)
{
    polyName="";
    QString aux("_");
    polygonName.append(aux);
    polygonName.append(QString::number(index));
    polyName=polygonName;

}

///////////////////////////////////////

void Polygon::setNameFound(QString polyname)
{
    polyName=polyname;
}

///////////////////////////////////////

void Polygon::setInstantiableState(bool state)
{
    instanStateP=state;
}

///////////////////////////////////////

void Polygon::setBoundingBox(QPointF topLeft, QPointF bottonRight)
{
    QRectF rectan(topLeft, bottonRight);
    BBox=rectan;
}

///////////////////////////////////////

Point Polygon::getPolyVertex(int index)
{
    return polyVertexes[index];
}

///////////////////////////////////////

vector<Point> Polygon::getPolyVertexes()
{
    return polyVertexes;
}

///////////////////////////////////////

int Polygon::getPolyNumVert()
{
    return polyVertexes.size();
}

///////////////////////////////////////

QColor Polygon::getPolyColor()
{
    return polyColor;
}

///////////////////////////////////////

QString Polygon::getPolyName()
{
    return polyName;
}

///////////////////////////////////////

bool Polygon::getInstantiableState()
{
    return instanStateP;
}

///////////////////////////////////////

QRectF Polygon::getBoundingBox()
{
    return BBox;
}

///////////////////////////////////////

void Polygon::modificatedPolygon(double factor){

    for(int i=0; i<polyVertexes.size(); i++){
        polyVertexes[i]=polyVertexes[i]*factor;
    }
}

///////////////////////////////////////

void Polygon::deleteVertexes()
{
    polyVertexes.clear();
}

///////////////////////////////////////

void Polygon::modificatedBBox(double factor)
{
    QPointF topLeft = BBox.topLeft();
    QPointF bottonRight = BBox.bottomRight();

    topLeft = topLeft*factor;
    bottonRight = bottonRight*factor;

    QRectF rectan(topLeft, bottonRight);
    BBox = rectan;
}
///////////////////////////////////////
