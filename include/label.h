#ifndef LABEL
#define LABEL

#include <QWidget>
#include <iostream>
#include <QMessageBox>
#include <opencv2/opencv.hpp>
#include "polygon.h"
#include "instantiable.h"
#include "noinstantiable.h"
#include <QVector>
#include <QPolygonF>

using namespace std;
using namespace cv;

class Polygon;

class Label
{

protected:
    int label_id;
    QString label_name;
    QColor label_color;
    bool label_instantiable;
    bool label_boundingbox;
    QVector<QRectF> my_BBox;

    vector<Point> my_Vertexes; // OpenCV point class
    vector<Polygon*> my_Polygones;
    double factor;
    bool polygonCreated;
    Polygon poly;
    QVector <QPointF> my_VertexesF;
    QVector <QPolygonF> my_PolygonesF;

public:
    Label();
    Label(QString name, int id, QColor color, bool instanciable, bool boundingbox);
    Label(const Label &Lbl);
    ~Label();

    int getLabelID();
    QString getLabelName();
    QColor getLabelColor();
    bool getLabelInstantiableState();
    bool getLabelBoundingBoxState();

    void setLabelID(int new_ID);
    void setLabelName(QString new_name);
    void setLabelColor(QColor new_color);
    void setLabelInstantiableState(bool new_instanc_state);
    void setLabelBoundingBoxState(bool new_bbox_state);

    void addVertex(Point newVertex);
    void addVertexF(QPointF newVertex);
    void deleteLastVertex();
    void deleteLastVertexF();
    Point getVertex(int index);
    vector<Point> getVertexes();
    QVector<QPointF> getVertexesF();
    int getNumVertexes();
    void setPolygonName(int index);
    void setPolyID(int index, int id);
    void setNameFound(int index, QString name);
    void setPolygonBBox(int index, QPointF topLeft, QPointF bottonRight);

    void modificatedVertex(double factor);
    void modificatedVertexF(double factor);
    void modificatedPolygon(double factor);
    void modificatedBBoxP(double factor);

    Instantiable *createPolygonI(int polygonID, QRectF BoundingBox);
    NoInstantiable* createPolygonNO();
    void addPolygon(Polygon poly);
    Polygon* getPolygon(int index);
    QPolygonF getPolygonF(int index);
    vector<Polygon *> getPolygones();
    QVector<QPolygonF> getPolygonesF();
    int getNumPolygones();
    int getNumPolygonesF();
    QString getPolygonName(int index);
    void deletePolygon(int index);
    void deletePolygonF(int index);
    void clearPolygon(int index);
    void clearPolygonF(int index);
    int getPolyID(int index);
    void setPolygon(Instantiable *poly);

    void saveQPolygon(QPolygonF poly);



};
#endif // LABEL
