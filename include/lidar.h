#ifndef LIDAR_H
#define LIDAR_H
#define PCL_NO_PRECOMPILE

#include <iostream>
#include <vector>
#include <math.h>       /* cos */
#include <fstream>
#include <sstream>
#include <iomanip>
#include <termios.h>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
//#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/passthrough.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/transforms.h>

#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core/eigen.hpp>

#include <QImage>
#include <QDir>
#include <sys/types.h>
#include <dirent.h>
#include "label.h"
#include "polygon.h"
#include "lidarinformation.h"
#include <pcl/impl/point_types.hpp>

using namespace cv;
using namespace std;
using namespace Eigen;


class Lidar
{

protected:
    vector<Point2f> point2D;
    vector<Point3d> point3D;
    vector<Point2f> point2DOriginal;
    vector<Point3d> point3DOriginal;
    MatrixXf matrixVeloToImg;
    Mat imageFilePoints;
    vector<lidarinformation> my_lidarInformation2D;
    vector<pcl::PointXYZRGB> my_lidarInformation3D;
    bool fullMatrix;
    bool fullMatrixLabels;

     public:
        Lidar();
        Lidar(vector<Point2f> point2d, vector<Point3d> point3d, MatrixXf matrixImg, Mat imagen);

        Mat showCalibrationMatrix(Mat img, bool fullMatrixAux, QString pathLidar, QString pathCalib, QString auxNameFile, MatrixXf matrixVeloAux, bool lidarBoolean);
        pcl::PointCloud<pcl::PointXYZI>::Ptr getPointCloud(string path);
        MatrixXf readCalibVelodyne(string pathCalib);
        MatrixXf readCalibCameraLeft(string pathCalib, MatrixXf velodyneResult);

        void setMatrix(MatrixXf matrixVelodyne);
        vector<Point2f> saveVector2d();
        vector<Point3d> saveVector3d();
        void setVectorLidar(vector<lidarinformation> lidarVector);
        vector<pcl::PointXYZRGB> getVector3DLabel();
        void setFullMatrix(bool aux);

        Mat getImage();
        bool getFullMatrix();
        vector<lidarinformation> getVectorLidarInformation();
        MatrixXf getMatrix();
        void setPoint2D(vector<Point2f> point2d);
        void setPoint3D(vector<Point3d> point3d);
        void setPoint3DLabel(vector<pcl::PointXYZRGB> pointLabel);


};

#endif // LIDAR_H
