#ifndef POLYGON
#define POLYGON

#include <QWidget>
#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;


class Polygon
{
public:
    vector <Point> polyVertexes;
    QColor polyColor;
    QString polyName;
    int numPoly;
    bool instanStateP;
    int polyID;
    QRectF BBox;

//public:
    Polygon();
    Polygon(vector<Point> vertexes, QColor color, QString polygonName, int numPoly, bool instantiableState, int polygonID, QRectF BBox);
    Polygon(const Polygon &Plg);
    ~Polygon();

    void setPolyName(int index, QString polygonName);
    void setNameFound(QString polyname);
    void setInstantiableState(bool state);
    void setBoundingBox(QPointF topLeft, QPointF bottonRight);

    Point getPolyVertex(int index);
    vector<Point> getPolyVertexes();
    int getPolyNumVert();
    QColor getPolyColor();
    QString getPolyName();
    bool getInstantiableState();
    QRectF getBoundingBox();

    void modificatedPolygon(double factor);
    void deleteVertexes();
    void modificatedBBox(double factor);

};

#endif // POLYGON
