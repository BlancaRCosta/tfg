#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <QDeclarativeItem>
#include <QMainWindow>
#include <QObject>
#include <QQuickItem>
#include <QSharedDataPointer>
#include <QWidget>

class ImageViewerData;

class ImageViewer
{
public:
    ImageViewer();
    ImageViewer(const ImageViewer &);
    ImageViewer &operator=(const ImageViewer &);
    ~ImageViewer();

private:
    QSharedDataPointer<ImageViewerData> data;
};

#endif // IMAGEVIEWER_H