﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#ifndef QT_NO_PRINTER
#include <QtPrintSupport/QPrinter>
#endif

#include <QtWidgets>
#include <QFileDialog>
#include <QMessageBox>
#include <qtablewidget.h>
#include <opencv2/opencv.hpp>
#include <iostream>
#include "tinyxml2.h"
#include "label.h"
#include "labeldialog.h"
#include "datasetdialog.h"
#include "viewlabel.h"
#include <QDir>
#include "instantiable.h"
#include "noinstantiable.h"
#include <sys/types.h>
#include <dirent.h>
#include "lidar.h"
#include <QString>
#include <QVector>
#include <QPolygon>
#include "lidarinformation.h"
#include <pcl/impl/point_types.hpp>
#include <QFile>
#include <qtextstream.h>

using namespace std;
using namespace cv;
using namespace tinyxml2;

#ifndef QT_NO_PRINTER
#include <QtPrintSupport/QPrintDialog>
#endif

class Label;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void getInterval();
    void nextImage();
    void prevImage();
    void update_currentFrame();

    bool scaledImage();
    void setLimits();
    bool searchPolygonesFile();

    void mousePressEvent(QMouseEvent *doubleclick);
    void paint();

    void zoom(double factor);
    void normalSize();

private Q_SLOTS:
    void openImage();
    void openVideo();
    void openLabels();
    void exportDatasetAction();
    void exportLabelsAction();    
    void on_exportLabelPointsCloud_triggered();
  //  void exportLabelPointsCloudAction();

    void on_prevButton_clicked();
    void on_nextButton_clicked();

    void on_labellingButton_clicked(bool checked);
    void on_deleteVertexButton_clicked();

    void on_addButton_clicked();
    void on_editButton_clicked();
    void on_deleteButton_clicked();

    void on_zoomIn_clicked();
    void on_normalSizeAct_clicked();
    void on_zoomOut_clicked();  

    void on_listWidget_clicked(const QModelIndex &index);
    void on_listWidget_doubleClicked(const QModelIndex &index);

    void on_deletePolygon_clicked();
    void on_tablePolygones_doubleClicked(const QModelIndex &index);
    void on_tablePolygones_cellChanged(int row, int column);
    void on_tablePolygones_clicked(const QModelIndex &index);

    void on_Interpolate_clicked(bool checked);

    void on_showLidarButton_clicked(bool checked);
    void on_mode3D_clicked(bool checked);
    void on_mode2D_clicked(bool checked);

    double modificatedFactorX();
    double modificatedFactorY();


private:

    Ui::MainWindow *ui;
    QLabel *label_image;
    QScrollArea *scrollArea;

    QAction *openImageAct;
    QAction *openVideoAct;
    QAction *openLabelsAct;
    QAction *exportDatasetAct;
    QAction *exportLabelsAct;
    QAction *exportLabelPointsCloudAct;
    QPushButton *labellingButton;
    QPushButton *interpolationButton;
    QPushButton *lidarButton;
    QPushButton *mode2DButton;
    QPushButton *mode3DButton;
    QPushButton *deletePolygon;

    // General variables
    char auxmenu; // Video o image
    QString fileName;
    QString folderName, imagesName;
    QString pathName;

    // Images variables
    QStringList imageFilesNames;
    QString imageFilesName;
    QImage imageFile,imageFileOriginal;
    int image_index; // Current image
    int num_image;
    Mat cvImg; // OpenCV image
    Mat imgFile, imgFileOriginal;

    // Video variables
    int interval;
    VideoCapture cap;
    double frame_msec;
    Mat frame;
    int num_frame, current_frame, contFrames;

    // Limit of image variables
    QPoint P1, P2;
    int height_image, width_image;
    int height_label, width_label;
    int height_imageOriginal, width_imageOriginal;

    // Labels variables
    vector<Label> my_Labels;
    QVector<vector<Label> > image_Labels;
    int index_label; // Index of current Label Selected
    vector<Polygon*> polygonesLabel;
    int polygonID;

    // Labelling variables
    bool labelling_checked;
    bool labelSelected;
    Mat imageDrawn;
    bool newPolygnsSaved;
    bool newLabelsSaved;
    bool introductedImage;
    bool videoOption;

    //Zoom variables
    double appliedFactor;   //applied factor to the image
    double accumulatedFactor;   //accumulated factor
    bool zoomActivated;

    //polygonesTable variables
    bool polygonEdit;
    int indexPolygon;

    //Interpolation variables
    QPointF topLeftD, topRightD, bottonLeftD, bottonRightD;
    Instantiable* polySelectedFirst;
    Instantiable* polyAuxInterpolation;
    Instantiable* polySelectedLast;
    int contInterval;

    //Lidar
    bool lidar_checked;
    QString pathImages, pathLidar, pathCalib;
    QString auxNameFile;
    vector<Point2f> point2D;
    vector<Point3d> point3D;
    bool fullMatrix;
    MatrixXf matrixVeloToImg;
    QVector<Lidar>  infoImages;
    vector<lidarinformation> my_lidarInformation2D;
    QPalette::ColorRole colorButtonLidar;
    bool mode2D_check;
    bool mode3D_check;
    pcl::PointCloud<pcl::PointXYZRGB> my_lidarInformation3D;

    //Scale original image
    float factorX, factorY;
};

#endif // MAINWINDOW_H
