#ifndef VIEWLABEL_H
#define VIEWLABEL_H

#include <QDialog>
#include "label.h"

namespace Ui {
class viewlabel;
}

class viewlabel : public QDialog
{
    Q_OBJECT

public:
    explicit viewlabel(QWidget *parent = 0);
    ~viewlabel();

    void setLabel(Label current_label);

private:
    Ui::viewlabel *ui;
};

#endif // VIEWLABEL_H
