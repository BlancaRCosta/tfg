#ifndef DATASETDIALOG_H
#define DATASETDIALOG_H

#include <QDialog>
#include <QColorDialog>
#include <QTextEdit>
#include <QPalette>
#include <QMessageBox>
#include <iostream>
#include "label.h"

class Label;

namespace Ui {
class DatasetDialog;
}

class DatasetDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DatasetDialog(QWidget *parent = 0);
    ~DatasetDialog();
    bool* getSelection();
    void enableCurrentFrame();
    bool getSelectedState();

private Q_SLOTS:
    void on_selectionButton_clicked();


private:
    Ui::DatasetDialog *ui;
    bool selection[5]; // [0] = colorOutput, [1] = IdOutput, [2] = instanciableOutput, [3] = polygonesOutput, [4] = currentFrame
    bool selectedState;
};

#endif // DATASETDIALOG_H
