#ifndef INSTANTIABLE_H
#define INSTANTIABLE_H
#include "polygon.h"

class Instantiable: public Polygon
{
    QRectF rectangle;

public:
    Instantiable();
    Instantiable(vector<Point> vertexes, QColor color, QString polygonName, int numPoly, bool instantiableState, int polygonID, QRectF BBox);
    Instantiable(const Instantiable &Plg);
    Instantiable(Instantiable* instan);
    ~Instantiable();

    void setPolyID(int id);
    int getPolyID();

    void setBoundingBoxI();
    void setBoundingBoxInterpolation(QPointF topLeft, QPointF bottonRight);
    QRectF getBoundingBoxI();
    void modificatedBBoxI(double factor);

};

#endif // INSTANTIABLE_H
