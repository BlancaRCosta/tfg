#ifndef NOINSTANTIABLE_H
#define NOINSTANTIABLE_H
#include "polygon.h"


class NoInstantiable: public Polygon
{
public:
    NoInstantiable();
    NoInstantiable(vector<Point> vertexes, QColor color, QString polygonName, int numPoly, bool instantiableState, int polygonID);
    NoInstantiable(const NoInstantiable &Plg);

    int getPolyID();
    void setPolyID(int id);
};

#endif // NOINSTANTIABLE_H
