#ifndef lidarinformation_H
#define lidarinformation_H

#include "label.h"


class lidarinformation: public Label
{
    int labelID;
    QColor labelColor;
    Point point;

    public:
        lidarinformation();

        void setLabelID(int id);
        void setLabelColor(QColor color);
        void setPoint(QPointF pointF);

        Point getVertex(int index);
        int getNumVertexes();
        int getLabelID();
        QColor getLabelColor();
        Point getPoint();

};

#endif // lidarinformation_H
