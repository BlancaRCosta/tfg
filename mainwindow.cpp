﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

///////////////////////////////////////

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Menu Options
    openImageAct = ui->openImageAct;
    openVideoAct = ui->openVideoAct;
    openLabelsAct = ui->openLabelsAct;
    exportDatasetAct = ui->exportDatasetAct;
    exportLabelsAct = ui->exportLabelsAct;
    labellingButton = ui->labellingButton;
    interpolationButton = ui->Interpolate;
    lidarButton = ui->showLidarButton;
    exportLabelPointsCloudAct = ui->exportLabelPointsCloud;
    deletePolygon = ui->deletePolygon;
    mode3DButton = ui->mode3D;

    // Displaying Image Label
    label_image = new QLabel;
    label_image->setBackgroundRole(QPalette::Base);
    label_image->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    label_image->setScaledContents(true);

    scrollArea = new QScrollArea;
    scrollArea->setWidget(label_image);

    // Initialize variables
    labelling_checked=false;
    labelSelected=false;
    index_label=-1;
    newPolygnsSaved=false;
    newLabelsSaved=false;
    auxmenu='n';
    videoOption=false;

    zoomActivated=false;
    introductedImage=true;
    accumulatedFactor=1.0;
    appliedFactor=1.0;
    polygonID=1;
    contFrames=1;
    contInterval=0;
    lidar_checked=false;
    fullMatrix=false;

    setWindowTitle(tr("annotat3d"));

    //Table polygones
    ui->tablePolygones->setColumnCount(2);
    QStringList titles;
    titles<< "Name" << "ID";
    ui->tablePolygones->setHorizontalHeaderLabels(titles);
    ui->tablePolygones->setShowGrid(false);
    polygonEdit=false;
    ui->tablePolygones->setColumnWidth(0,185);
    ui->tablePolygones->setColumnWidth(1,125);

    //interpolationButton->setEnabled(false);

    /*
     * CONNECT SLOTS AND FUNCTIONS
     */

    // Menu Options
    connect(openImageAct,SIGNAL(triggered()),this,SLOT(openImage()));
    connect(openVideoAct,SIGNAL(triggered()),this,SLOT(openVideo()));
    connect(openLabelsAct,SIGNAL(triggered()),this,SLOT(openLabels()));
    connect(exportDatasetAct,SIGNAL(triggered()),this,SLOT(exportDatasetAction()));
    connect(exportLabelsAct,SIGNAL(triggered()),this,SLOT(exportLabelsAction()));
    connect(exportLabelPointsCloudAct, SIGNAL(triggered),this,SLOT(exportLabelPointsCloudAction()));
   // connect(labelButton, SIGNAL(clicked()),this,SLOT(on_labellingButton_clicked(bool)));

}

///////////////////////////////////////

MainWindow::~MainWindow()
{
    delete ui;
}

///////////////////////////////////////

void MainWindow::openImage()
{
    if(introductedImage==true && newLabelsSaved==true)
    {
        QMessageBox newDecision;
        newDecision.setWindowTitle("New image");
        newDecision.setText("Do you want to go to open a new image?");
        newDecision.setInformativeText("If you change without saving Labels and Polygones, you'll lose this information.");
        newDecision.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        newDecision.setDefaultButton(QMessageBox::No);
        newDecision.setIcon(QMessageBox::Question);
        int ret = newDecision.exec();

        if (ret == QMessageBox::No)
            return;

        else if (ret == QMessageBox::Yes){
              newPolygnsSaved=true;
              newLabelsSaved=false;


              // Clean former vertexes and polygones
              int numVertexes, numPolygones;

              for (int i=0; i<my_Labels.size(); i++)
              {
                  // Delete all vertexes
                  //image_Labels.clear();
                  numVertexes=my_Labels[i].getNumVertexes();
                  for (int j=0; j<numVertexes; j++)
                      my_Labels[i].deleteLastVertex();

                  // Delete all polygones
                  numPolygones=my_Labels[i].getNumPolygones();
                  for (int k=0; k<numPolygones; k++)
                      my_Labels[i].deletePolygon(k);
              }
              //Delete vector of list and table widget
              my_Labels.clear();
              polygonesLabel.clear();
              ui->listWidget->clear();
              for(int r=0; r<ui->tablePolygones->rowCount(); r++)
              {
                  ui->tablePolygones->removeRow(r);
              }
        }
    }

    auxmenu='i';
    introductedImage=true;

    // Load all files names

    fileName = QFileDialog::getExistingDirectory(this,("Open folder"),QDir::currentPath());
    imagesName = fileName;

    QString aux("/images");
    pathImages = fileName;
    pathImages = pathImages.append(aux);
    QDir dirImages(pathImages);

    aux="/lidar";
    pathLidar = fileName;
    pathLidar = pathLidar.append(aux);
    QDir dirLidar(pathLidar);

    aux="/calib";
    pathCalib = fileName;
    pathCalib = pathCalib.append(aux);
    QDir dirCalib(pathCalib);

    //Check wich folders exists
    if(dirImages.exists() && !dirCalib.exists() && !dirLidar.exists())
    {
        lidarButton->setVisible(false);
        mode3DButton->setVisible(false);
    }
    else if(dirImages.exists() && dirCalib.exists() && dirLidar.exists())
    {
        lidarButton->setVisible(true);
        mode3DButton->setVisible(true);
    }

    //open folder selected
    DIR *dir = opendir(pathImages.toStdString().data());
    //set the new path for the content of the directory
    string path = pathImages.toStdString().append("/");

    if(NULL == dir)
    {
        std::cout << "could not open directory: " << path.c_str() << std::endl;
        return;
    }

    struct dirent *readPathImages = readdir(dir);
    while (readPathImages = readdir (dir))
    {
        if (readPathImages == NULL)
            cout << "\nERROR! pent could not be initialised correctly";
        QString auxImagesFiles(readPathImages->d_name);
        QString auxPathImages = pathImages;
        aux="/";
        auxPathImages=auxPathImages.append(aux);
        auxPathImages=auxPathImages.append(auxImagesFiles);
        if(auxImagesFiles=="." || auxImagesFiles==".." || auxImagesFiles == folderName)
            continue;
        else
            imageFilesNames.append(auxPathImages);
    }
    closedir(dir);

  //  imageFilesNames = QFileDialog::getOpenFileNames(this, tr("Open Image"), dirImages.absolutePath(), tr("Images (*.png *.jpeg *.xpm *.jpg)"));

    //puts the images in alphabetical' order
    sort(imageFilesNames.begin(), imageFilesNames.end());

    num_image = imageFilesNames.size();
    if (num_image==0)
        return;

    image_Labels.resize(num_image);
    infoImages.resize(num_image);

    // Load first image
    image_index=0;
    fileName=imageFilesNames.at(image_index);
    size_t found = fileName.toStdString().find_last_of("/\\");
    auxNameFile = QString::fromStdString(fileName.toStdString().substr(found+1));
    auxNameFile=auxNameFile.section(".",0,0);

    if (!fileName.isEmpty())
    {
        ui->frameLabel->setText("Image");

        //Store the image current to don't load it every moment
        imageFileOriginal.load(fileName);
        imgFileOriginal=cv::imread(fileName.toStdString());
        imgFileOriginal.copyTo(imgFile);

        if (imageFileOriginal.isNull())
        {
            QMessageBox::information(this, tr("Image Viewer"), tr("Cannot load %1.").arg(fileName));
            return;
        }

        // Check dimensions
        height_label = ui->labelImage->height();
        width_label = ui->labelImage->width();

        height_image = height_imageOriginal = imageFileOriginal.height();
        width_image = width_imageOriginal = imageFileOriginal.width();


        bool scaleImage = scaledImage();
        if(scaleImage) // Scale to label dimensions
        {
            imageFileOriginal=imageFileOriginal.scaled(ui->labelImage->size(), Qt::KeepAspectRatio);
            imageFile=imageFileOriginal;
           // image=image.scaledToHeight(ui->labelImage->height());
           // image=image.scaledToWidth(ui->labelImage->width());
            height_image = imageFileOriginal.height();
            width_image = imageFileOriginal.width();

            height_imageOriginal=imageFileOriginal.height();
            width_imageOriginal=imageFileOriginal.width();
        }

        // Set labelling limits
        setLimits();

        // Get Mat image
        QImage   swapped = imageFileOriginal;
        if ( imageFileOriginal.format() == QImage::Format_RGB32 )
            swapped = swapped.convertToFormat(QImage::Format_RGB888);

        swapped = swapped.rgbSwapped();

        cvImg = Mat(swapped.height(), swapped.width(),CV_8UC3,const_cast<uchar*>(swapped.bits()),
                    static_cast<size_t>(swapped.bytesPerLine())).clone();

        // Show image
        ui->labelImage->setPixmap(QPixmap::fromImage(imageFileOriginal));
        ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

        label_image->show();

        // Set current image and total image labels
        QString numimage_string = QString::number(num_image,10);
        ui->numFramesLabel->setText(numimage_string);
        update_currentFrame();

        // Search for polygones file
        bool polygonesLoad = searchPolygonesFile();

        if (polygonesLoad==true)
            paint();
    }
    else
        return;

    if(num_image>1)
    {
        QMessageBox newDecision;
        newDecision.setWindowTitle("Charges image");
        newDecision.setText("Do you want to keep the labels information in the next images?");
     //   newDecision.setInformativeText("If you change without saving Polygones, you'll lose this information.");
        newDecision.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        newDecision.setDefaultButton(QMessageBox::No);
        newDecision.setIcon(QMessageBox::Question);
        int ret = newDecision.exec();

        if (ret == QMessageBox::No)
          videoOption=false;

        else if (ret == QMessageBox::Yes)
          videoOption=true;
    }

}

///////////////////////////////////////

void MainWindow:: openVideo()
{
    if(newPolygnsSaved==true)
    {
        QMessageBox newDecision;
        newDecision.setWindowTitle("New video");
        newDecision.setText("Do you want to go to open a new video?");
        newDecision.setInformativeText("If you change without saving Polygones, you'll lose this information.");
        newDecision.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        newDecision.setDefaultButton(QMessageBox::No);
        newDecision.setIcon(QMessageBox::Question);
        int ret = newDecision.exec();

        if (ret == QMessageBox::No)
            return;

        else if (ret == QMessageBox::Yes)
            newPolygnsSaved=true;
    }

    // Clean former vertexes and polygones
    int numVertexes, numPolygones;

    for (int i=0; i<my_Labels.size(); i++)
    {
        // Delete all vertexes
        numVertexes=my_Labels[i].getNumVertexes();
        for (int j=0; j<numVertexes; j++)
            my_Labels[i].deleteLastVertex();

        // Delete all polygones
        numPolygones=my_Labels[i].getNumPolygones();
        for (int k=0; k<numPolygones; k++)
            my_Labels[i].deletePolygon(k);
    }
    polygonesLabel.clear();
    ui->listWidget->clear();
    for(int r=0; r<ui->tablePolygones->rowCount(); r++)
    {
        ui->tablePolygones->removeRow(r);
    }


    fileName = QFileDialog::getOpenFileName(this, tr("Import Video"), QDir::currentPath(), tr("Videos (*.avi *.mp4 *.mpg)"));
    auxmenu='v';
    videoOption=true;

    //Saves the path to the image's folder
    String fileNameAux = fileName.toStdString();
    size_t found = fileNameAux.find_last_of("/\\");
    string path= fileNameAux.substr(0, found);
    pathName = QString::fromStdString(path);

    QDir dir;
    dir.setPath(QString::fromStdString(fileNameAux));

    //Saves the name of video to name the images's folder
    folderName = QString::fromStdString(fileNameAux.substr(found+1));
    imagesName = folderName;
    folderName=folderName.section(".",0,0);
    QString aux("Frames");
    folderName=folderName.append(aux);

    if (!fileName.isEmpty())
    {
        ui->frameLabel->setText("Frame");

        //name of the video
        string name= fileName.toStdString();

        //Load video
        cap.open(name);
        //Relative position to end of video
        cap.set(CV_CAP_PROP_POS_AVI_RATIO,1);

        if(!cap.isOpened())
        {
            cout<< "error in VideoCatpure: check path file" << endl;
            return;
        }
        else
        {
            QString path = pathName;
            aux="/";
            path=path.append(aux);
            path=path.append(folderName);
            aux="/images";
            path=path.append(aux);

            //Access to the route of the imageFiles's folder
            QFile fileFrames;
            dir.setPath(path);
            fileFrames.setFileName(folderName);

            bool folderCreated;

            //If the folder was already created
            if(dir.exists())
            {
                QMessageBox newDecission;
                newDecission.setText("The folder is exist yet. Select the frames to work with them");
                newDecission.exec();
                cap.release();

                folderCreated=true;
                fileFrames.close();

                //Open the folder with the video's images
                imageFilesNames = QFileDialog::getOpenFileNames(this, tr("Open Image"), dir.absolutePath(),tr("Images (*.png *.jpeg *.xpm *.jpg)"));
                num_image=imageFilesNames.size();

                introductedImage=true;

                if (num_image==0)
                    return;

                // Load first image
                image_index=0;
                fileName=imageFilesNames.at(image_index);

                if (!fileName.isEmpty())
                {
                  ui->frameLabel->setText("Frame");

                  imageFileOriginal.load(fileName);

                  if (imageFileOriginal.isNull())
                  {
                     QMessageBox::information(this, tr("Image Viewer"), tr("Cannot load %1.").arg(fileName));
                     return;
                   }

                  // Check dimensions
                   height_label = ui->labelImage->height();
                   width_label = ui->labelImage->width();

                   height_image=height_imageOriginal = imageFileOriginal.height();
                   width_image=width_imageOriginal = imageFileOriginal.width();

                   bool scaleImage = scaledImage();
                   if(scaleImage) // Scale to label dimensions
                   {
                        imageFileOriginal=imageFileOriginal.scaled(ui->labelImage->size(), Qt::KeepAspectRatio);
                        height_image = imageFileOriginal.height();
                        width_image = imageFileOriginal.width();

                        height_imageOriginal=imageFileOriginal.height();
                        width_imageOriginal=imageFileOriginal.width();
                   }

                    // Set labelling limits
                    setLimits();

                    // Get Mat image
                    QImage   swapped = imageFileOriginal;
                    if ( imageFile.format() == QImage::Format_RGB32 )
                        swapped = swapped.convertToFormat(QImage::Format_RGB888);

                    swapped = swapped.rgbSwapped();

                    cvImg = Mat(swapped.height(), swapped.width(),CV_8UC3,const_cast<uchar*>(swapped.bits()),
                                static_cast<size_t>(swapped.bytesPerLine())).clone();

                    // Show image
                    ui->labelImage->setPixmap(QPixmap::fromImage(imageFileOriginal));
                    ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

                    label_image->show();

                    // Set current image and total image labels
                    ui->numFramesLabel->setText(QString::number(num_image));
                    update_currentFrame();

                    // Search for polygones file
                    bool polygonesLoad = searchPolygonesFile();

                    if (polygonesLoad==true)
                        paint();
                }
                else
                    return;
            }

            //If the folder was not created, the program create it
            else
            {
                //Create the folder of the images
                dir.setPath(pathName);
                folderCreated=dir.mkpath(folderName);
                QString aux("/");
                pathName = pathName.append(aux);
                pathName = pathName.append(folderName);
                dir.setPath(pathName);
                folderCreated=dir.mkpath("images");

                dir.setCurrent(path);

                 //Saves the images in the folder
                  if(fileFrames.open(QIODevice::WriteOnly))
                  {
                    // Calculate number of msec per frame.
                    // (msec/sec / frames/sec = msec/frame)
                    frame_msec = 1000 / cap.get(CV_CAP_PROP_FPS);

                    //count frames
                    num_image = cap.get(CV_CAP_PROP_POS_FRAMES);
                   // num_frame = cap.get(CV_CAP_PROP_FRAME_COUNT);
                    current_frame = 1;

                    ui->numFramesLabel->setText(QString::number(num_image));

                    QMessageBox newDecission;
                    newDecission.setText("The frames are been saving. Close the windows and see the counter");
                    newDecission.exec();

                    //Read the frames and saves in images JPEG
                    while(contFrames<=num_image)
                    {
                        cap.set(CV_CAP_PROP_POS_FRAMES, contFrames);
                        cap>>frame;

                       if(!cap.read(frame))
                        {
                            cout << "Cannot read the frame from file"<< contFrames << endl;
                            contFrames++;
                            continue;
                        }
                        else
                        {
                            QImage videoFrame(frame.data, frame.cols, frame.rows, frame.step, QImage::Format_RGB888);
                            QImage videoFrameRGB=videoFrame.rgbSwapped();

                            // Check dimensions
                            height_label = ui->labelImage->height();
                            width_label = ui->labelImage->width();
                            height_image = videoFrameRGB.height();
                            width_image = videoFrameRGB.width();

                            bool scaleImage = scaledImage();
                            if(scaleImage) // Scale to label dimensions
                            {
                                videoFrameRGB = videoFrameRGB.scaled(ui->labelImage->size(), Qt::KeepAspectRatio);
                                height_image = videoFrameRGB.height();
                                width_image = videoFrameRGB.width();
                                Size resizeMat(width_image, height_image);
                                cv::resize(frame, frame, resizeMat);
                            }

                            // Set labelling limits
                            setLimits();

                            if (waitKey(frame_msec) >= 0)
                                return;

                            update_currentFrame();
                            ui->currentFrameLabel->setText(QString::number(contFrames));

                            QString frameName=folderName;
                            QString frameNumber;
                            QString aux("_");
                            frameName=frameName.append(aux);
                            frameNumber = QString::number(current_frame, 6);
                                if(frameNumber.size()<7) frameNumber = QString(7-frameNumber.size(), '0')+frameNumber;

                            frameName = frameName.append(frameNumber);
                            aux=".jpg";
                            frameName=frameName.append(aux);

                            videoFrameRGB.save(frameName,"JPG");

                            current_frame++;
                            contFrames++;
                            folderCreated=true;

                        }
                    }

                    fileFrames.close();
                    newDecission.setText("Update finished.");
                    newDecission.exec();
                    cap.release();  //close the video

                  //  current_frame=1;

                    introductedImage=true;

                    if (num_image==0)
                        return;

                      ui->frameLabel->setText("Frame");

                      //Open the folder with the video's images
                     // imageFilesNames = QFileDialog::getOpenFileNames(this, tr("Open Image"), QDir::currentPath(),tr("Images (*.png *.jpeg *.xpm *.jpg)"));
                      //open folder selected
                      QString auxPath = QDir::currentPath();
                      DIR *dir = opendir(auxPath.toStdString().data());
                      //set the new path for the content of the directory
                      string path = auxPath.toStdString().append("/");

                      if(NULL == dir)
                      {
                          std::cout << "could not open directory: " << path.c_str() << std::endl;
                          return;
                      }

                      struct dirent *readPathImages = readdir(dir);
                      while (readPathImages = readdir (dir))
                      {
                          if (readPathImages == NULL)
                              cout << "\nERROR! pent could not be initialised correctly";
                          QString auxImagesFiles(readPathImages->d_name);
                          QString auxPathImages = auxPath;
                          aux="/";
                          auxPathImages=auxPathImages.append(aux);
                          auxPathImages=auxPathImages.append(auxImagesFiles);
                          if(auxImagesFiles=="." || auxImagesFiles==".." || auxImagesFiles==folderName)
                              continue;
                          else
                              imageFilesNames.append(auxPathImages);
                      }
                      closedir(dir);

                    //  imageFilesNames = QFileDialog::getOpenFileNames(this, tr("Open Image"), dirImages.absolutePath(), tr("Images (*.png *.jpeg *.xpm *.jpg)"));

                      //puts the images in alphabetical' order
                      sort(imageFilesNames.begin(), imageFilesNames.end());

                      num_image=imageFilesNames.size();

                      introductedImage=true;

                      if (num_image==0)
                          return;

                      // Load first image
                      image_index=0;
                      fileName=imageFilesNames.at(image_index);

                      if (!fileName.isEmpty())
                      {
                        ui->frameLabel->setText("Frame");

                        imageFileOriginal.load(fileName);

                        if (imageFileOriginal.isNull())
                        {
                           QMessageBox::information(this, tr("Image Viewer"), tr("Cannot load %1.").arg(fileName));
                           return;
                         }

                        // Check dimensions
                         height_label = ui->labelImage->height();
                         width_label = ui->labelImage->width();

                         height_image=height_imageOriginal = imageFileOriginal.height();
                         width_image=width_imageOriginal = imageFileOriginal.width();

                         bool scaleImage = scaledImage();
                         if(scaleImage) // Scale to label dimensions
                         {
                              imageFileOriginal=imageFileOriginal.scaled(ui->labelImage->size(), Qt::KeepAspectRatio);
                              height_image = imageFileOriginal.height();
                              width_image = imageFileOriginal.width();

                              height_imageOriginal=imageFileOriginal.height();
                              width_imageOriginal=imageFileOriginal.width();
                         }

                          // Set labelling limits
                          setLimits();

                          // Get Mat image
                          QImage   swapped = imageFileOriginal;
                          if ( imageFileOriginal.format() == QImage::Format_RGB32 )
                              swapped = swapped.convertToFormat(QImage::Format_RGB888);

                          swapped = swapped.rgbSwapped();

                          cvImg = Mat(swapped.height(), swapped.width(),CV_8UC3,const_cast<uchar*>(swapped.bits()),
                                      static_cast<size_t>(swapped.bytesPerLine())).clone();

                          // Show image
                          ui->labelImage->setPixmap(QPixmap::fromImage(imageFileOriginal));
                          ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

                          label_image->show();

                          // Set current image and total image labels
                          ui->numFramesLabel->setText(QString::number(num_image));
                          update_currentFrame();
                       }
                      else
                          cout<< "Error. The route was not accessed"<<endl;
                }
                if(!folderCreated){
                    QMessageBox newD;
                    newD.setText("Error. The folder was not created.");
                    newD.exec();
                }
            }
        }
    }
    else
        return;
}

///////////////////////////////////////

bool MainWindow::scaledImage()
{
    int diffHeight = height_image - height_label;
    int diffWidth = width_image - width_label;

    if (diffHeight<=0 && diffWidth<=0)
        return false;
    else // Need to be scaled
        return true;
}

///////////////////////////////////////

bool MainWindow::searchPolygonesFile()
{
    // File Name
    string fileNamePolyString = fileName.toStdString();
    int sizeFileName = fileNamePolyString.length();
    fileNamePolyString.erase(sizeFileName-4); // Dirección sin el .jpg, .png, .avi, .mpg

    if (auxmenu == 'v')
    {
        fileNamePolyString = fileNamePolyString + '_';

        // Add current frame to the name of the file
        stringstream stringCurrentFrame;
        stringCurrentFrame << fileNamePolyString << current_frame;

        fileNamePolyString = stringCurrentFrame.str();
    }

    fileNamePolyString = fileNamePolyString + "_Polygones.xml";

    const char *fileNamePolyChar = fileNamePolyString.c_str();

    // Load file
    XMLDocument xml_openPolygones;
    XMLError result = xml_openPolygones.LoadFile(fileNamePolyChar);
    if (result != tinyxml2::XML_SUCCESS)
        return false;

    // File found
    QMessageBox newDecision;
    newDecision.setWindowTitle("Polygones file");
    newDecision.setText("There is a file related to this image that contains labeled polygons.");
    newDecision.setInformativeText("Do you want to load it?");
    newDecision.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    newDecision.setDefaultButton(QMessageBox::Yes);
    newDecision.setIcon(QMessageBox::Question);
    int ret = newDecision.exec();

    if (ret == QMessageBox::No)
        return false;

    else if (ret == QMessageBox::Yes)
    {
        // If there're current Labels
        if (my_Labels.size()>0)
        {
            //Initialize Selected Label Area and index_label
            labelSelected=false;
            index_label=-1;

            // Make a Decision
            QMessageBox new_Decision;
            new_Decision.setWindowTitle("New Labels");
            new_Decision.setText("Do you want to replace current Labels with new related-to-polygons Labels?");
            new_Decision.setInformativeText("If you click 'No', the new tags will be added below the current ones");
            new_Decision.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            new_Decision.setDefaultButton(QMessageBox::Yes);
            new_Decision.setIcon(QMessageBox::Question);
            int ret = new_Decision.exec();

            if (ret == QMessageBox::Yes)
            {
                my_Labels.clear();
                ui->listWidget->clear();
            }
            else if (ret == QMessageBox::No)
            {
                // Delete polygones of previous images
                int numPolygones;

                for (int i=0; i<my_Labels.size(); i++)
                {
                    // Delete all polygones from other images
                    numPolygones=my_Labels[i].getNumPolygones();
                    for (int k=0; k<numPolygones; k++)
                        my_Labels[i].deletePolygon(k);
                }
            }
        }

        XMLNode *pRoot = xml_openPolygones.FirstChild();

        // Variables
        Label *loadLabel;
        QString *nameLabel;
        string nameLabelString;
        int numLabels, IDLabel, redLabel, greenLabel, blueLabel, numPolygns, numVertexes, Xcoord, Ycoord;
        QColor colorLabel;
        bool instLabel, bboxLabel;
        Polygon *loadPolygon;
        vector<Point> loadVertexes;
        Point *loadVertex;

        XMLElement *pLabelElement, *pNameElement, *pIDElement, *pColorElement, *pColorLevel, *pInstElement, *pBBoxElement,
                *pNumPolygnsELement, *pPolygonElement, *pNumVertexElement, *pVertexElement, *pXVertex, *pYVertex;

        // Num of Labels
        XMLElement *pNumLabelElement = pRoot->FirstChildElement("NumLabels");
        pNumLabelElement->QueryIntText(&numLabels);

        // Get Labels attributes
        for(pLabelElement= pRoot->FirstChildElement("Label"); pLabelElement !=NULL;
            pLabelElement=pLabelElement->NextSiblingElement("Label"))
        {
            // Name
            pNameElement = pLabelElement->FirstChildElement("Name");
            nameLabelString = pNameElement->GetText();
            nameLabel = new QString(nameLabelString.c_str());

            // ID
            pIDElement = pLabelElement->FirstChildElement("ID");
            pIDElement->QueryIntText(&IDLabel);

            // Color
            pColorElement = pLabelElement->FirstChildElement("Color");

            pColorLevel = pColorElement->FirstChildElement("Red");
            pColorLevel->QueryIntText(&redLabel);
            colorLabel.setRed(redLabel);

            pColorLevel = pColorElement->FirstChildElement("Green");
            pColorLevel->QueryIntText(&greenLabel);
            colorLabel.setGreen(greenLabel);

            pColorLevel = pColorElement->FirstChildElement("Blue");
            pColorLevel->QueryIntText(&blueLabel);
            colorLabel.setBlue(blueLabel);

            // Instanciable
            pInstElement = pLabelElement->FirstChildElement("Instanciable");
            pInstElement->QueryBoolText(&instLabel);

            // Bounding Box
            pBBoxElement = pLabelElement->FirstChildElement("BoundingBox");
            pBBoxElement->QueryBoolText(&bboxLabel);

            // Create label
            loadLabel = new Label(*nameLabel, IDLabel, colorLabel, instLabel, bboxLabel);

            // Add to Labels vector and List Widget
            my_Labels.push_back(*loadLabel);
            ui->listWidget->addItem(*nameLabel);

            // Num of Polygones
            pNumPolygnsELement = pLabelElement->FirstChildElement("NumPolygones");
            pNumPolygnsELement->QueryIntText(&numPolygns);

            // Get Polygones attributes
            for(pPolygonElement = pLabelElement->FirstChildElement("Polygon"); pPolygonElement !=NULL;
                pPolygonElement = pPolygonElement->NextSiblingElement("Polygon"))
            {
                // Num of Vertexes
                pNumVertexElement = pPolygonElement->FirstChildElement("NumVertexes");
                pNumVertexElement->QueryIntText(&numVertexes);

                // Get All Vertexes
                for(pVertexElement = pPolygonElement->FirstChildElement("Vertex"); pVertexElement !=NULL;
                    pVertexElement = pVertexElement->NextSiblingElement("Vertex"))
                {
                    // Get X coordinate
                    pXVertex = pVertexElement->FirstChildElement("X");
                    pXVertex->QueryIntText(&Xcoord);

                    // Get Y coordinate
                    pYVertex = pVertexElement->FirstChildElement("Y");
                    pYVertex->QueryIntText(&Ycoord);

                    // Create vertex
                    loadVertex = new Point(Xcoord, Ycoord);

                    // Add to load Vertexes vector
                    loadVertexes.push_back(*loadVertex);
                }

                // Create polygon
                //loadPolygon = new Polygon(loadVertexes, colorLabel);

                // Add to Polygones vector in Label
                my_Labels.back().addPolygon(*loadPolygon);

                // Clear Vertex vector
                loadVertexes.clear();
            }
        }

        return true;
    }
}


///////////////////////////////////////

void MainWindow::openLabels()
{
    QString fileLabelsName = QFileDialog::getOpenFileName(this, tr("Open Labels File"), QDir::currentPath(), tr("XML files (*.xml)"));

    if (!fileLabelsName.isEmpty())
    {
        string fileLabelsNameString = fileLabelsName.toStdString();
        const char *fileLabelsNameChar = fileLabelsNameString.c_str();

        // Load file
        XMLDocument xml_openLabels;
        xml_openLabels.LoadFile(fileLabelsNameChar);

        XMLNode *pRoot = xml_openLabels.FirstChild();

        // Num of Labels
        int numLabels;
        XMLElement *pNumLabelElement = pRoot->FirstChildElement("NumLabels");
        pNumLabelElement->QueryIntText(&numLabels);

        // Variables
        Label *loadLabel;
        QString *nameLabel;
        string nameLabelString;
        int IDLabel, redLabel, greenLabel, blueLabel;
        QColor colorLabel;
        bool instLabel, bboxLabel;

        XMLElement *pNameElement, *pIDElement, *pColorElement, *pColorLevel, *pInstElement, *pBBoxElement;

        // Get Labels attributes
        for(XMLElement *pLabelElement= pRoot->FirstChildElement("Label"); pLabelElement !=NULL;
            pLabelElement=pLabelElement->NextSiblingElement("Label"))
        {
            // Name
            pNameElement = pLabelElement->FirstChildElement("Name");
            nameLabelString = pNameElement->GetText();
            nameLabel = new QString(nameLabelString.c_str());

            // ID
            pIDElement = pLabelElement->FirstChildElement("ID");
            pIDElement->QueryIntText(&IDLabel);

            // Color
            pColorElement = pLabelElement->FirstChildElement("Color");

            pColorLevel = pColorElement->FirstChildElement("Red");
            pColorLevel->QueryIntText(&redLabel);
            colorLabel.setRed(redLabel);

            pColorLevel = pColorElement->FirstChildElement("Green");
            pColorLevel->QueryIntText(&greenLabel);
            colorLabel.setGreen(greenLabel);

            pColorLevel = pColorElement->FirstChildElement("Blue");
            pColorLevel->QueryIntText(&blueLabel);
            colorLabel.setBlue(blueLabel);

            // Instanciable
            pInstElement = pLabelElement->FirstChildElement("Instanciable");
            pInstElement->QueryBoolText(&instLabel);

            // Bounding Box
            pBBoxElement = pLabelElement->FirstChildElement("BoundingBox");
            pBBoxElement->QueryBoolText(&bboxLabel);

            // Create label
            loadLabel = new Label(*nameLabel, IDLabel, colorLabel, instLabel, bboxLabel);

            // Add to Labels vector and List Widget
            my_Labels.push_back(*loadLabel);
            ui->listWidget->addItem(*nameLabel);
        }
    }
}

///////////////////////////////////////

void MainWindow::setLimits()
{
    // IMAGE LABEL POSITION: x 13  y 27

    QPoint C;
    C.setX(13+width_label/2);
    C.setY(27+height_label/2); //54

    P1.setX(C.x()-width_image/2);
    P1.setY(C.y()-height_image/2);

    P2.setX(C.x()+width_image/2);
    P2.setY(C.y()+height_image/2);


}


///////////////////////////////////////

void MainWindow::on_prevButton_clicked()
{
    zoomActivated=false;
    normalSize();
    fullMatrix=false;
   //  if(labellingButton->isEnabled())
     //   labellingButton->click();

    //Show the elements of 2D mode
    ui->listWidget->show();
    ui->Labels_label->show();
    ui->addButton->show();
    ui->editButton->show();
    ui->deleteButton->show();

    ui->label->show();
    ui->tablePolygones->show();
    ui->deletePolygon->show();
    ui->normalSizeAct->show();
    ui->zoomIn->show();
    ui->zoomOut->show();


    if (num_image<=1)
       return;

   // Check frame interval
    getInterval();

  //Saves the information about the previous image's label
     if(newLabelsSaved)
     {
         image_Labels[image_index].clear();
         for(int i=0; i<my_Labels.size(); i++)
         {
             image_Labels[image_index].push_back(my_Labels[i]);
         }
     }
     ui->listWidget->clear();
     polygonesLabel.clear();
     my_lidarInformation2D.clear();
     point2D.clear();
     point3D.clear();

     int row=ui->tablePolygones->rowCount();
     for(int r=0; r<row; r++)
     {
         ui->tablePolygones->takeItem(r,0);
         ui->tablePolygones->takeItem(r,1);
     }
     for(int r=0; r<=row; r++)
     {
         ui->tablePolygones->removeRow(ui->tablePolygones->rowCount()-1);
     }
     // Clean former vertexes and polygones
     int numVertexes, numPolygones, numQPolygones;
     for (int i=0; i<my_Labels.size(); i++)
     {
         // Delete all vertexes
         numVertexes=my_Labels[i].getNumVertexes();
         for (int j=0; j<numVertexes; j++)
             my_Labels[i].deleteLastVertex();

         // Delete all polygones
         numPolygones=my_Labels[i].getNumPolygones();
         for (int k=numPolygones-1; k>=0; k--)
             my_Labels[i].clearPolygon(k);

         //Delete all QPolygones
         numQPolygones=my_Labels[i].getNumPolygonesF();
         for(int p=numQPolygones-1; p>=0; p--)
             my_Labels[i].clearPolygonF(p);
     }
      my_Labels.clear();

   //Charges the information about the currently image's labels
   prevImage();
   int j=0;

   if(!image_Labels[image_index].empty())
   {
       my_Labels=image_Labels[image_index];

       for(int i=0; i<my_Labels.size(); i++)
       {
           ui->listWidget->addItem(my_Labels[i].getLabelName());
           int numPoly= my_Labels[i].getNumPolygones();

           if(my_Labels[i].getLabelInstantiableState())
           {
               while(j<numPoly)
               {
                  Polygon *poly = new Polygon(*(my_Labels[i].getPolygon(j)));
                  Instantiable *polyInstan = (Instantiable*) poly;
                  polygonesLabel.push_back(polyInstan);
                  ui->tablePolygones->insertRow(ui->tablePolygones->rowCount());
                  ui->tablePolygones->setItem(ui->tablePolygones->rowCount()-1,0,new QTableWidgetItem(polyInstan->getPolyName()));
                  ui->tablePolygones->setItem(ui->tablePolygones->rowCount()-1,1,new QTableWidgetItem(QString::number(polyInstan->getPolyID())));

                  j++;
               }
               j=0;
           }
           else
           {
               while(j<numPoly)
               {
                  Polygon *poly = new Polygon(*(my_Labels[i].getPolygon(j)));
                  NoInstantiable *polyNoInstan = (NoInstantiable*) poly;
                  polygonesLabel.push_back(polyNoInstan);
                  ui->tablePolygones->insertRow(ui->tablePolygones->rowCount());
                  ui->tablePolygones->setItem(ui->tablePolygones->rowCount()-1,0,new QTableWidgetItem(polyNoInstan->getPolyName()));
                  ui->tablePolygones->setItem(ui->tablePolygones->rowCount()-1,1,new QTableWidgetItem(""));

                  j++;
               }
               j=0;
           }
       }

       paint();
   }

   if(!infoImages[image_index].getVectorLidarInformation().empty())
   {
       my_lidarInformation2D = infoImages[image_index].getVectorLidarInformation();
       point2D = infoImages[image_index].saveVector2d();
       point3D = infoImages[image_index].saveVector3d();
       fullMatrix = true;
       matrixVeloToImg = infoImages[image_index].getMatrix();
   }
}

///////////////////////////////////////

void MainWindow::prevImage()
{
    // Get new position considering the value of the interval
    image_index = image_index - interval;

    if (image_index < 0)
    {
        int overflow = num_image + image_index;
        image_index = overflow;
    }

    //Load prev file name
    fileName=imageFilesNames.at(image_index);

    if (!fileName.isEmpty())
    {
        imageFileOriginal.load(fileName);
        imgFileOriginal=cv::imread(fileName.toStdString());
        imgFileOriginal.copyTo(imgFile);

        if (imageFileOriginal.isNull())
        {
            QMessageBox::information(this, tr("Image Viewer"), tr("Cannot load %1.").arg(fileName));
            return;
        }

        // Check dimensions
        height_label = ui->labelImage->height();
        width_label = ui->labelImage->width();
        height_image = imageFileOriginal.height();
        width_image = imageFileOriginal.width();

        height_imageOriginal =imageFileOriginal.height();
        width_imageOriginal=imageFileOriginal.width();

        bool scaleImage = scaledImage();
        if(scaleImage) // Scale to label dimensions
        {
            imageFileOriginal=imageFile=imageFileOriginal.scaled(ui->labelImage->size(), Qt::KeepAspectRatio);
            height_image =height_imageOriginal= imageFileOriginal.height();
            width_image = width_imageOriginal= imageFileOriginal.width();
        }

        // Set labelling limits
        setLimits();

        // Get Mat image
        QImage   swapped = imageFileOriginal;
        if ( imageFileOriginal.format() == QImage::Format_RGB32 )
            swapped = swapped.convertToFormat(QImage::Format_RGB888);

        swapped = swapped.rgbSwapped();

        cvImg = Mat(swapped.height(), swapped.width(),CV_8UC3,const_cast<uchar*>(swapped.bits()),
                    static_cast<size_t>(swapped.bytesPerLine())).clone();

        // Show image
        ui->labelImage->setPixmap(QPixmap::fromImage(imageFileOriginal));
        ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

        label_image->show();

        // Set current image and total image labels
        QString numimage_string = QString::number(num_image,10);
        ui->numFramesLabel->setText(numimage_string);
        update_currentFrame();

        // Search for polygones file
        bool polygonesLoad = searchPolygonesFile();

        if (polygonesLoad==true)
            paint();
    }
}

///////////////////////////////////////

void MainWindow::on_nextButton_clicked()
{
    zoomActivated=false;
    normalSize();
    fullMatrix=false;
  //  if(labellingButton->isEnabled())
     //   labellingButton->click();

    //Show the elements of 2D mode
    ui->listWidget->show();
    ui->Labels_label->show();
    ui->addButton->show();
    ui->editButton->show();
    ui->deleteButton->show();

    ui->label->show();
    ui->tablePolygones->show();
    ui->deletePolygon->show();
    ui->normalSizeAct->show();
    ui->zoomIn->show();
    ui->zoomOut->show();


     if (num_image<=1)
        return;

    // Check frame interval
     getInterval();

   //Saves the information about the previous image's label
      if(newLabelsSaved)
      {
          image_Labels[image_index].clear();
          for(int i=0; i<my_Labels.size(); i++)
          {
              image_Labels[image_index].push_back(my_Labels[i]);
          }
      }
      ui->listWidget->clear();
      polygonesLabel.clear();
      my_lidarInformation2D.clear();
      point2D.clear();
      point3D.clear();

      int row=ui->tablePolygones->rowCount();
      for(int r=0; r<row; r++)
      {
          ui->tablePolygones->takeItem(r,0);
          ui->tablePolygones->takeItem(r,1);
      }
      for(int r=0; r<=row; r++)
      {
          ui->tablePolygones->removeRow(ui->tablePolygones->rowCount()-1);
      }
      // Clean former vertexes and polygones
      int numVertexes, numPolygones, numQPolygones;
      for (int i=0; i<my_Labels.size(); i++)
      {
          // Delete all vertexes
          numVertexes=my_Labels[i].getNumVertexes();
          for (int j=0; j<numVertexes; j++)
              my_Labels[i].deleteLastVertex();

          // Delete all polygones
          numPolygones=my_Labels[i].getNumPolygones();
          for (int k=numPolygones-1; k>=0; k--)
              my_Labels[i].clearPolygon(k);

          //Delete all QPolygones
          numQPolygones=my_Labels[i].getNumPolygonesF();
          for(int p=numQPolygones-1; p>=0; p--)
              my_Labels[i].clearPolygonF(p);
      }
       my_Labels.clear();

    // Load the information about the currently image's labels
    nextImage();
    int j=0;

    if(!image_Labels[image_index].empty())
    {
        for(int i=0; i<image_Labels[image_index].size(); i++)
        {
            my_Labels.push_back(image_Labels[image_index][i]);
        }
        for(int i=0; i<my_Labels.size(); i++)
        {
            ui->listWidget->addItem(my_Labels[i].getLabelName());
            int numPoly= my_Labels[i].getNumPolygones();

            if(my_Labels[i].getLabelInstantiableState())
            {
                while(j<numPoly)
                {
                   Polygon *poly = new Polygon(*(my_Labels[i].getPolygon(j)));
                   Instantiable *polyInstan = (Instantiable*) poly;
                   polygonesLabel.push_back(polyInstan);

                   ui->tablePolygones->insertRow(ui->tablePolygones->rowCount());
                   ui->tablePolygones->setItem(ui->tablePolygones->rowCount()-1,0,new QTableWidgetItem(polyInstan->getPolyName()));
                   ui->tablePolygones->setItem(ui->tablePolygones->rowCount()-1,1,new QTableWidgetItem(QString::number(polyInstan->getPolyID())));

                   j++;
                }
                j=0;
            }
            else
            {
                while(j<numPoly)
                {
                   Polygon *poly = new Polygon(*(my_Labels[i].getPolygon(j)));
                   NoInstantiable *polyNoInstan = (NoInstantiable*) poly;

                   polygonesLabel.push_back(polyNoInstan);
                   ui->tablePolygones->insertRow(ui->tablePolygones->rowCount());
                   ui->tablePolygones->setItem(ui->tablePolygones->rowCount()-1,0,new QTableWidgetItem(polyNoInstan->getPolyName()));
                   ui->tablePolygones->setItem(ui->tablePolygones->rowCount()-1,1,new QTableWidgetItem(""));

                   j++;
                }
                j=0;
            }
        }

        paint();
    }

    if(!infoImages[image_index].getVectorLidarInformation().empty())
    {
        my_lidarInformation2D=infoImages[image_index].getVectorLidarInformation();
        point2D = infoImages[image_index].saveVector2d();
        point3D = infoImages[image_index].saveVector3d();
        fullMatrix = true;
        matrixVeloToImg = infoImages[image_index].getMatrix();
    }
}

///////////////////////////////////////

void MainWindow::nextImage()
{
    // Get new position considering the value of the interval
    image_index = image_index + interval;

    if (image_index > (num_image-1))
    {
        int overflow = image_index - num_image;
        image_index = overflow;
    }

    //Load next file name
    fileName=imageFilesNames.at(image_index);

    if (!fileName.isEmpty())
    {
        imageFileOriginal.load(fileName);
        imgFileOriginal=cv::imread(fileName.toStdString());
        imgFileOriginal.copyTo(imgFile);

        if (imageFileOriginal.isNull())
        {
            QMessageBox::information(this, tr("Image Viewer"), tr("Cannot load %1.").arg(fileName));
            return;
        }

        // Check dimensions
        height_label = ui->labelImage->height();
        width_label = ui->labelImage->width();
        height_image = imageFileOriginal.height();
        width_image = imageFileOriginal.width();

        height_imageOriginal =imageFileOriginal.height();
        width_imageOriginal=imageFileOriginal.width();

        bool scaleImage = scaledImage();
        if(scaleImage) // Scale to label dimensions
        {
            imageFile=imageFileOriginal=imageFileOriginal.scaled(ui->labelImage->size(), Qt::KeepAspectRatio);
            height_image = height_imageOriginal=imageFileOriginal.height();
            width_image = width_imageOriginal = imageFileOriginal.width();

        }

        // Set labelling limits
        setLimits();

        // Get Mat image
        QImage   swapped = imageFileOriginal;
        if ( imageFileOriginal.format() == QImage::Format_RGB32 )
            swapped = swapped.convertToFormat(QImage::Format_RGB888);

        swapped = swapped.rgbSwapped();

        cvImg = Mat(swapped.height(), swapped.width(),CV_8UC3,const_cast<uchar*>(swapped.bits()),
                    static_cast<size_t>(swapped.bytesPerLine())).clone();

        // Show image
        ui->labelImage->setPixmap(QPixmap::fromImage(imageFileOriginal));
        ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

        label_image->show();

        // Set current image label
        QString numimage_string = QString::number(num_image,10);
        ui->numFramesLabel->setText(numimage_string);
        update_currentFrame();

        // Search for polygones file
        bool polygonesLoad = searchPolygonesFile();

        if (polygonesLoad==true)
            paint();
    }
}

///////////////////////////////////////

void MainWindow::getInterval()
{
    //transform num of interval frames from QString to int
    QString textInterval = ui -> lineInterval -> text();
    bool ok;
    interval=textInterval.toInt(&ok,10);
}

///////////////////////////////////////

void MainWindow::update_currentFrame()
{
    QString current_string;

    current_string = QString::number(image_index+1,10);

    ui->currentFrameLabel->setText(current_string);
}

///////////////////////////////////////

void MainWindow::on_labellingButton_clicked(bool checked)
{
    // Checked = true si está activado, false si está desactivado
    labelling_checked = checked;
    if(checked)
    {
      //  labellingButton->setEnabled(true);
        setStyleSheet("QPushButton:checked { background-color: orange; border-style: double; border-width: 7px; border-radius: 10px;}");
    }
    else
    {
       // labellingButton->setEnabled(false);
        labellingButton->repaint();
        normalSize();
    }
 }

///////////////////////////////////////

void MainWindow::mousePressEvent(QMouseEvent *doubleclick)
{
    if (labelling_checked==false)
        return;

    if (labelSelected==false)
    {
        QMessageBox::warning(this, tr("Labelling error"), tr("Please, select a valid Label"));
        return;
    }

    // Create new Qt Vertex
    QPoint *new_vertex = new QPoint;
    *new_vertex = doubleclick->pos();

    if((new_vertex->x() < P1.x()) || (new_vertex->x() > P2.x()))
    {
        QMessageBox::warning(this, tr("Labelling error"), tr("Please, set new vertex within the image limits"));
        return;
    }
    else if((new_vertex->y() < P1.y()) || (new_vertex->y() > P2.y()))
    {
        QMessageBox::warning(this, tr("Labelling error"), tr("Please, set new vertex within the image limits"));
        return;
    }
    else{
        // Create OpenCV Vertex

        // Calculate vertex position regarding the reference system of the image (P1 point)
        int Xcv = new_vertex->x() - P1.x();
        int Ycv = new_vertex->y() - P1.y();

        Point *cv_vertex = new Point (Xcv, Ycv);
        QPointF *cv_vertexF = new QPointF (Xcv, Ycv);

        int numVertexes = my_Labels[index_label].getNumVertexes();
        bool vertexes_match=false;

        if (numVertexes>0) // Ya se ha creado el primer vértice
        {
            Point aux_vertex = my_Labels[index_label].getVertex(0);

            // Check if the new Vertex match with the first Vertex
            if ((Xcv>=aux_vertex.x-10 && Xcv<=aux_vertex.x+10) && (Ycv>=aux_vertex.y-10 && Ycv<=aux_vertex.y+10))
                vertexes_match=true;
        }

        newPolygnsSaved = false;

        if(vertexes_match==false || vertexes_match==true && my_Labels[index_label].getNumVertexes()<3)
        { 
            // Add Vertex to vector
            my_Labels[index_label].addVertex(*cv_vertex);

            cv_vertexF->setX(cv_vertexF->x()*modificatedFactorX());
            cv_vertexF->setY(cv_vertexF->y()*modificatedFactorY());
            my_Labels[index_label].addVertexF(*cv_vertexF);

            // Draw Vertex
            paint();
        }
        else
        {
            QMessageBox newDecision;
            newDecision.setWindowTitle("Create new polygon");
            newDecision.setText("Do you want to create a polygon?");
            newDecision.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            newDecision.setDefaultButton(QMessageBox::Yes);
            newDecision.setIcon(QMessageBox::Question);
            int ret = newDecision.exec();

            if (ret == QMessageBox::Yes)
            {
               newPolygnsSaved=true;
               if(my_Labels[index_label].getLabelInstantiableState())
               {
                    // Create and add Polygon to the label
                   vector<Point> my_Vertexes = my_Labels[index_label].getVertexes();
                   QRectF BoundingBox;
                   Rect BBox = boundingRect(my_Vertexes);
                   qreal x_BBox,  y_BBox, width_BBox, height_BBox;
                   x_BBox= BBox.x;
                   y_BBox= BBox.y;
                   width_BBox=BBox.width;
                   height_BBox=BBox.height;
                   BoundingBox.setRect(x_BBox,y_BBox,width_BBox,height_BBox);

                   QVector<QPointF> my_VertexesF = my_Labels[index_label].getVertexesF();
                   QPolygonF polyPaint (my_VertexesF);
                   my_Labels[index_label].saveQPolygon(polyPaint);

                   Instantiable *polyInstan=my_Labels[index_label].createPolygonI(polygonID, BoundingBox);
                   polygonID++;

                   //Saves the created polygons and add to the tablePolygones
                   polygonesLabel.push_back(polyInstan);
                   ui->tablePolygones->insertRow(ui->tablePolygones->rowCount());
                   ui->tablePolygones->setItem(ui->tablePolygones->rowCount()-1,0,new QTableWidgetItem(polyInstan->getPolyName()));
                   ui->tablePolygones->setItem(ui->tablePolygones->rowCount()-1,1,new QTableWidgetItem(QString::number(polyInstan->getPolyID())));

                    // Draw new Polygon
                    paint();

                }
               else
               {
                   // Create and add Polygon to the label
                  NoInstantiable *polyNoInstan=my_Labels[index_label].createPolygonNO();

                  QVector<QPointF> my_VertexesF = my_Labels[index_label].getVertexesF();
                  QPolygonF polyPaint (my_VertexesF);
                  my_Labels[index_label].saveQPolygon(polyPaint);

                  //Saves the created polygons and add to the tablePolygones
                  polygonesLabel.push_back(polyNoInstan);
                  ui->tablePolygones->insertRow(ui->tablePolygones->rowCount());
                  ui->tablePolygones->setItem(ui->tablePolygones->rowCount()-1,0,new QTableWidgetItem(polyNoInstan->getPolyName()));
                  ui->tablePolygones->setItem(ui->tablePolygones->rowCount()-1,1,new QTableWidgetItem(""));

                   // Draw new Polygon
                   paint();
               }
            }
            else if (ret == QMessageBox::No)
            {
                // Add Vertex to vector
                my_Labels[index_label].addVertex(*cv_vertex);
                my_Labels[index_label].addVertexF(*cv_vertexF);

                // Draw new Vertex
                paint();
            }
        }
    }
}

///////////////////////////////////////

void MainWindow::paint()
{
    Mat imgDrawn=cvImg.clone();

    // Draw all existing polygones
    QColor label_color;
    int num_polygones_label;
    Polygon* label_polygon;
    int num_vertexes_polygon;

    for (int i=0; i<my_Labels.size(); i++)
    {
        num_polygones_label=my_Labels[i].getNumPolygones();
        label_color=my_Labels[i].getLabelColor();

        for (int j=0; j<num_polygones_label; j++)
        {
            label_polygon = my_Labels[i].getPolygon(j);

            if(my_Labels[i].getLabelBoundingBoxState() && label_polygon->getPolyVertexes().empty())
            {

                   Instantiable* instan = (Instantiable*) label_polygon;
                   Rect BBox(instan->getBoundingBox().x(), instan->getBoundingBox().y(),instan->getBoundingBox().width(),
                             instan->getBoundingBox().height());
                   rectangle(imgDrawn, BBox, Scalar(0,0,0), 2);
            }

            else if(my_Labels[i].getLabelBoundingBoxState() && !label_polygon->getPolyVertexes().empty())
            {
                num_vertexes_polygon = label_polygon->getPolyNumVert();
                int *pNumVertex = &num_vertexes_polygon;

                Point poly_Vertexes[num_vertexes_polygon];
                for (int k=0; k<num_vertexes_polygon; k++){
                     poly_Vertexes[k] = label_polygon->getPolyVertex(k);
                }
                const Point *pVertex = poly_Vertexes;
                const Point **pPointer = &pVertex;

                fillPoly(imgDrawn, pPointer, pNumVertex, 1,
                               Scalar(label_color.blue(), label_color.green(), label_color.red()));

                Instantiable* instan = (Instantiable*) label_polygon;
                Rect BBox(instan->getBoundingBox().x(), instan->getBoundingBox().y(),instan->getBoundingBox().width(),
                          instan->getBoundingBox().height());
                rectangle(imgDrawn, BBox, Scalar(0,0,0), 2);
            }

            else
            {
                num_vertexes_polygon = label_polygon->getPolyNumVert();
                int *pNumVertex = &num_vertexes_polygon;

                Point poly_Vertexes[num_vertexes_polygon];
                for (int k=0; k<num_vertexes_polygon; k++){
                     poly_Vertexes[k] = label_polygon->getPolyVertex(k);
                }
                const Point *pVertex = poly_Vertexes;
                const Point **pPointer = &pVertex;

                fillPoly(imgDrawn, pPointer, pNumVertex, 1,
                               Scalar(label_color.blue(), label_color.green(), label_color.red()));
            }
        }
    }

    if(index_label>=0)
    {

        // Draw a circle to mark Vertexes and a line to connect them
        QColor color = my_Labels[index_label].getLabelColor();
        int numVertexes = my_Labels[index_label].getNumVertexes();

        for (int i=0; i<numVertexes; i++)
        {
            circle(imgDrawn, my_Labels[index_label].getVertex(i), 3, Scalar(0, 0, 0)); // Black edge
            circle(imgDrawn, my_Labels[index_label].getVertex(i), 2, Scalar(color.blue(), color.green(), color.red()), -1 ); // BGR format

            // Draw Lines if there are more than one vertex
            if(i>0)
            {

                line(imgDrawn, my_Labels[index_label].getVertex(i-1), my_Labels[index_label].getVertex(i), Scalar(0, 0, 0), 4);
                line(imgDrawn, my_Labels[index_label].getVertex(i-1), my_Labels[index_label].getVertex(i),
                     Scalar(color.blue(), color.green(), color.red()), 2);
            }
        }
    }

    // Show image in QLabel
    QImage vertexImage=QImage(imgDrawn.data, imgDrawn.cols, imgDrawn.rows, imgDrawn.step, QImage::Format_RGB888);
    QImage vertexImageRGB=vertexImage.rgbSwapped();

    if(zoomActivated){
        ui->labelImage->setPixmap(QPixmap::fromImage(vertexImageRGB));
        ui->labelImage->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    }
    else{
        ui->labelImage->setPixmap(QPixmap::fromImage(vertexImageRGB));
        ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
   }

    label_image->show();

    imgDrawn.release();
}

///////////////////////////////////////

void MainWindow::zoom(double factor)
{
     appliedFactor=factor;   

     // Check dimensions
     height_image = height_image*factor;
     width_image = width_image*factor;

     imageFile=imageFileOriginal.scaled(width_image, height_image,Qt::KeepAspectRatio, Qt::SmoothTransformation);

     height_label=imageFile.height();
     width_label=imageFile.width();

     // Set labelling limits
     setLimits();

     // Get Mat image
     QImage   swapped = imageFile;
     if ( imageFile.format() == QImage::Format_RGB32 )
         swapped = swapped.convertToFormat(QImage::Format_RGB888);

     swapped = swapped.rgbSwapped();

     cvImg = Mat(swapped.height(), swapped.width(),CV_8UC3,const_cast<uchar*>(swapped.bits()),
                 static_cast<size_t>(swapped.bytesPerLine())).clone();

     // Show image

     ui->labelImage->setPixmap(QPixmap::fromImage(imageFile));
     ui->labelImage->setAlignment(Qt::AlignLeft | Qt::AlignTop);

     label_image->show();
}

///////////////////////////////////////

void MainWindow::normalSize()
{
    // Check dimensions
    height_image=height_imageOriginal;
    width_image=width_imageOriginal;

    imageFile=imageFileOriginal;

    height_label=ui->labelImage->height();
    width_label=ui->labelImage->width();

    bool scaleImage = scaledImage();
    if(scaleImage) // Scale to label dimensions
    {
        imageFileOriginal=imageFileOriginal.scaled(ui->labelImage->size(), Qt::KeepAspectRatio);
        imageFile=imageFileOriginal;
       // image=image.scaledToHeight(ui->labelImage->height());
       // image=image.scaledToWidth(ui->labelImage->width());
        height_image = imageFileOriginal.height();
        width_image = imageFileOriginal.width();

        height_imageOriginal=imageFileOriginal.height();
        width_imageOriginal=imageFileOriginal.width();
    }

    // Set labelling limits
    setLimits();

    // Get Mat image
    QImage   swapped = imageFile;
    if ( imageFile.format() == QImage::Format_RGB32 )
        swapped = swapped.convertToFormat(QImage::Format_RGB888);

    swapped = swapped.rgbSwapped();

    cvImg = Mat(swapped.height(), swapped.width(),CV_8UC3,const_cast<uchar*>(swapped.bits()),
                static_cast<size_t>(swapped.bytesPerLine())).clone();


     // Show image
     ui->labelImage->setPixmap(QPixmap::fromImage(imageFile));
     ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

     label_image->show();
     double factor;

     for(int i=0; i<my_Labels.size(); i++)
     {
           factor=1/accumulatedFactor;
           my_Labels[i].modificatedVertex(factor);
           my_Labels[i].modificatedPolygon(factor);
           my_Labels[i].modificatedBBoxP(factor);

           //Change the value of bounding box created in the interpolation
           vector<Polygon*> polyAux = my_Labels[i].getPolygones();
           for(int p=0; p<polyAux.size(); p++)
           {
               if(my_Labels[i].getLabelBoundingBoxState() && polyAux[p]->getPolyVertexes().empty())
               {
                   polyAux[p]->modificatedBBox(factor);
                   Instantiable* instan = (Instantiable*) polyAux[p];
                   instan->modificatedBBoxI(factor);
               }
           }
     }

      paint();

      accumulatedFactor=1.0;
}

///////////////////////////////////////

void MainWindow::on_deleteVertexButton_clicked()
{
    int numVertexes = my_Labels[index_label].getNumVertexes();

    if (numVertexes == 0)
    {
        QMessageBox::warning(this, tr("Deleting error"), tr("There aren't more vertexes"));
        return;
    }

    my_Labels[index_label].deleteLastVertex();
    paint();
}

///////////////////////////////////////

void MainWindow::on_addButton_clicked()
{
    LabelDialog addLabel;

    addLabel.setMyLabels(my_Labels);
    addLabel.setWindowTitle("Add New Label");
    addLabel.setModal(true);
    addLabel.setMode(0);
    addLabel.exec();   

    // Get new Label attributes
    Label *newLabel = new Label;
    *newLabel=addLabel.getMyLabel();

    // Avoid error Labels
    QString name = newLabel->getLabelName();
    if (name.isEmpty())
        return;

    ui->listWidget->addItem(name);

    // Add Label to vector
    my_Labels.push_back(*newLabel);

    //Add Label in the all images
    if(videoOption)
    {
        for(int i=0; i<image_Labels.size(); i++)
        {
            image_Labels[i].push_back(*newLabel);
        }
    }

    newLabelsSaved=true;

}

///////////////////////////////////////

void MainWindow::on_editButton_clicked()
{
    int index = ui -> listWidget-> currentRow();
    if (index<0)
    {
        QMessageBox::warning(this, tr("Edit Label"), tr("Please, select a Label to edit"));
        return;
    }

    QString savedName=my_Labels[index].getLabelName();

    LabelDialog editLabel;
    editLabel.setWindowTitle("Edit Current Label");
    editLabel.setMyLabels(my_Labels);
    editLabel.setModal(true);
    editLabel.setMode(1);
    editLabel.setIndexLabel(index);
    editLabel.setLabelAttributes(my_Labels[index]);
    editLabel.exec();

    // Get edited Label attributes
    Label *editedLabel = new Label;
    *editedLabel=editLabel.getMyLabel();

    // Avoid error Labels
    QString name = editedLabel->getLabelName();
    if (name.isEmpty())
        return;

    //Saves instantiable state before edit the label
    bool instanStateSave= my_Labels[index].getLabelInstantiableState();

    // Modificate item in listWidget
    ui->listWidget->item(index)->setText(editedLabel->getLabelName());

    // Edited Label attributes
    my_Labels[index].setLabelName(editedLabel->getLabelName());
    my_Labels[index].setLabelID(editedLabel->getLabelID());
    my_Labels[index].setLabelColor(editedLabel->getLabelColor());
    my_Labels[index].setLabelInstantiableState(editedLabel->getLabelInstantiableState());
    my_Labels[index].setLabelBoundingBoxState(editedLabel->getLabelBoundingBoxState());

    for(int i=0; i<polygonesLabel.size(); i++)
    {
        if(polygonesLabel[i]->getPolyName().contains(savedName))
            polygonesLabel[i]->setInstantiableState(my_Labels[index_label].getLabelInstantiableState());
    }

    //Update the polygons name of vectors my_Label and polygonesLabel
    int numpolygonesLabel=my_Labels[index_label].getNumPolygones();
    int j=1;

    for(int i=0; i<numpolygonesLabel;i++)
    {
        my_Labels[index_label].setPolygonName(i);
    }

    for(int i=0; i<polygonesLabel.size(); i++)
    {
        if(polygonesLabel[i]->getPolyName().contains(savedName))
        {
            polygonesLabel[i]->setPolyName(j, my_Labels[index_label].getLabelName());
            j++;
        }
    }

    //Update instantiable state of the vector
    for(int i=0; i<my_Labels[index_label].getPolygones().size(); i++)
    {
        if(my_Labels[index_label].getLabelInstantiableState()==true)
        {
            Instantiable* instan = (Instantiable*) my_Labels[index_label].getPolygones()[i];
            instan->setInstantiableState(true);
            instan->setPolyID(polygonID);
            polygonID++;
        }
        else
        {
            NoInstantiable* NoInstan = (NoInstantiable*) my_Labels[index_label].getPolygones()[i];
            NoInstan->setInstantiableState(false);
            NoInstan->setPolyID(0);
        }
    }

    for(int i=0; i<polygonesLabel.size(); i++)
    {
        if(polygonesLabel[i]->getPolyName().contains(savedName) && polygonesLabel[i]->getInstantiableState() && instanStateSave==false)
         {
            Instantiable* instan = (Instantiable*) polygonesLabel[i];
            instan->setPolyID(my_Labels[index_label].getLabelInstantiableState());
        }

        else if(polygonesLabel[i]->getPolyName().contains(savedName) && polygonesLabel[i]->getInstantiableState()==false)
        {
            NoInstantiable* NoInstan = (NoInstantiable*) polygonesLabel[i];
            NoInstan->setPolyID(my_Labels[index_label].getLabelInstantiableState());
        }
    }
    //Update bounding box state of the vector
    for(int i=0; i<polygonesLabel.size(); i++)
    {
        if(my_Labels[index].getLabelBoundingBoxState()==true)
        {

            Instantiable* instanAux = (Instantiable*) my_Labels[index_label].getPolygones()[i];
            instanAux->setBoundingBoxI();

            //Include BoundingBox in polygonesLabel and my_Labels
            QRectF BBoxAux = instanAux->getBoundingBoxI();
            polygonesLabel[i]->setBoundingBox(BBoxAux.topLeft(),BBoxAux.bottomRight());
        }
    }

    image_Labels[image_index] = my_Labels;

    //Edit the label in the all images
    if(videoOption)
    {
        for(int i=0; i<image_Labels.size(); i++)
        {
           for(int j=0; j<image_Labels[i].size(); j++)
           {
              if(image_Labels[i][j].getLabelName().contains(savedName))
              {
                 image_Labels[i][j].setLabelName(my_Labels[index].getLabelName());
                 image_Labels[i][j].setLabelID(my_Labels[index].getLabelID());
                 image_Labels[i][j].setLabelColor(my_Labels[index].getLabelColor());
                 image_Labels[i][j].setLabelInstantiableState(my_Labels[index].getLabelInstantiableState());
                 image_Labels[i][j].setLabelBoundingBoxState(my_Labels[index].getLabelBoundingBoxState());
              }

              vector<Polygon*> polygonAux=image_Labels[i][j].getPolygones();

              int numPolygonesAux=image_Labels[i][j].getNumPolygones();
              int r=1;

              for(int t=0; t<numPolygonesAux;t++)
              {
                  image_Labels[i][j].setPolygonName(t);
              }

              for(int p=0; p<polygonAux.size(); p++)
              {
                  if(polygonAux[p]->getPolyName().contains(savedName))
                  {
                      polygonAux[p]->setPolyName(r, image_Labels[i][j].getLabelName());
                      p++;
                  }
              }

              //Update instantiable state
              for(int p=0; p<polygonAux.size(); p++)
              {
                  polygonAux[p]->setInstantiableState(image_Labels[i][j].getLabelInstantiableState());
              }

              for(int p=0; p<polygonAux.size(); p++)
              {
                  if(polygonAux[p]->getPolyName().contains(savedName) && polygonAux[p]->getInstantiableState() && instanStateSave==false)
                   {
                      Instantiable* instan = (Instantiable*) polygonAux[p];
                      instan->setPolyID(polygonID);
                      polygonID++;
                  }
              }

              //Update bounding box state
              for(int p=0; p<polygonAux.size(); p++)
              {
                  if(image_Labels[i][j].getLabelBoundingBoxState()==true)
                  {
                      /*Instantiable* instanAux = (Instantiable*) my_Labels[index_label].getPolygones()[p];
                      instanAux->setBoundingBoxI();

                      //Include BoundingBox in polygonesLabel and my_Labels
                      QRectF BBoxAux = instanAux->getBoundingBoxI();
                      image_Labels[i][j].setPolygonBo(BBoxAux.topLeft(),BBoxAux.bottomRight());*/
                      Instantiable* instan = (Instantiable*) polygonAux[p];
                      instan->setBoundingBoxI();
                      QRectF BBoxAux = instan->getBoundingBoxI();
                      polygonAux[p]->setBoundingBox(BBoxAux.topLeft(), BBoxAux.bottomRight());
                  }
              }
           }
        }

    // Update polygones and vertexes on image
    paint();

    //Clear and include the  polygons again
    for(int r=0; r<ui->tablePolygones->rowCount(); r++)
    {
        ui->tablePolygones->takeItem(r,0);
        ui->tablePolygones->takeItem(r,1);
    }
    int indexAux=0;
    while(indexAux <= index_label)
    {
        for(int i=0; i<polygonesLabel.size(); i++)
        {
           if(polygonesLabel[i]->getInstantiableState()==true)
           {
               Instantiable* instan = (Instantiable*) polygonesLabel[i];
               ui->tablePolygones->setItem(i,0,new QTableWidgetItem(instan->getPolyName()));
               ui->tablePolygones->setItem(i,1,new QTableWidgetItem(QString::number(instan->getPolyID())));
           }
           else
           {
                NoInstantiable *noInstan = (NoInstantiable*) polygonesLabel[i];
                ui->tablePolygones->setItem(i,0,new QTableWidgetItem(noInstan->getPolyName()));
           }
         }
       indexAux++;
    }
    }
}

///////////////////////////////////////

void MainWindow::on_deleteButton_clicked()
{
    int index = ui -> listWidget-> currentRow();
    if (index<0)
    {
        QMessageBox::warning(this, tr("Delete Label"), tr("Please, select a Label to delete"));
        return;
    }

    QString savedName=my_Labels[index_label].getLabelName();
    bool deletePoly;

    //Delete Label in the all images
    if(videoOption)
    {
        for(int i=0; i<image_Labels.size(); i++)
        {
           for(int j=0; j<image_Labels[i].size(); j++)
           {
              if(image_Labels[i][j].getLabelName().contains(savedName))
              {
                 if(image_Labels[i][j].getNumPolygones()>0)
                 {
                     QMessageBox::warning(this, tr("Delete Label"), tr("Exist polygones in other image. You can't delete this Label"));
                     deletePoly=false;
                     return;
                 }
                 else
                 {
                    deletePoly=true;
                     image_Labels[i].erase(image_Labels[i].begin()+index);
                 }
              }
           }
        }
    }

    if(deletePoly)
    {
        my_Labels.erase(my_Labels.begin()+index);
        delete ui -> listWidget -> currentItem();

        // If the Deleted Label has a lower index than index_label, update this variable
        if (index<index_label)
            index_label--;

        // Update polygones and vertexes on image
        paint();

        if(my_Labels.size()>0)
            newLabelsSaved=true;
        else
            newLabelsSaved=false;

        //Delete polygons of tablePolygones
        int contPolygonDelete=0;
        for(int i=0; i<polygonesLabel.size(); i++)
        {
            if(polygonesLabel[i]->getPolyName().contains(savedName))
            {
                polygonesLabel.erase(polygonesLabel.begin()+i);
                i--;
                contPolygonDelete++;
            }
        }

        //Clear and include the  polygons again
        for(int r=0; r<ui->tablePolygones->rowCount(); r++)
        {
            ui->tablePolygones->takeItem(r,0);
            ui->tablePolygones->takeItem(r,1);
        }
        int indexAux=0;
        while(indexAux <= index_label)
        {
            for(int i=0; i<polygonesLabel.size(); i++)
            {
               if(my_Labels[indexAux].getLabelInstantiableState())
               {
                   Instantiable* instan = (Instantiable*) polygonesLabel[i];
                   ui->tablePolygones->setItem(i,0,new QTableWidgetItem(instan->getPolyName()));
                   ui->tablePolygones->setItem(i,1,new QTableWidgetItem(QString::number(instan->getPolyID())));
               }
               else
               {
                    NoInstantiable *noInstan = (NoInstantiable*) polygonesLabel[i];
                    ui->tablePolygones->setItem(i,0,new QTableWidgetItem(noInstan->getPolyName()));
               }
             }
           indexAux++;
        }

        //Delete the empty rows
        for(int i=0; i<contPolygonDelete; i++)
        {
            ui->tablePolygones->removeRow(ui->tablePolygones->rowCount()-1);
        }
    }
}


///////////////////////////////////////
void MainWindow::exportDatasetAction()
{
    // Only export if there are an image or a video opened
    if(auxmenu=='n')
        return;

    DatasetDialog selectDataset;
    selectDataset.setWindowTitle("Select Dataset");
    selectDataset.setModal(true);
    if(auxmenu=='v')
        selectDataset.enableCurrentFrame();
    selectDataset.exec();

    bool selected_state = selectDataset.getSelectedState();

    if(!selected_state)
        return;

    for(int i=0; i<image_Labels.size(); i++)
    {
        if(!image_Labels[i].empty())
        {
            my_Labels = image_Labels[i];
            // Dataset options selected
            bool *pDataset = selectDataset.getSelection();

            bool colorOption=*pDataset;
            bool idOption=*(pDataset+1);
            bool instOption=*(pDataset+2);
            bool polygonesOption=*(pDataset+3);
            bool currentFrame = false;
            if(auxmenu=='v')
                currentFrame=*(pDataset+4);

            // Variables
            int num_labels = my_Labels.size();

            Polygon Lbl_polygon;
            int num_Lbl_polygns, num_plg_vertexes;

            // File Name
            string fileNameString = fileName.toStdString();
            int sizeFileName = fileNameString.length();
            fileNameString.erase(sizeFileName-4); // Dirección sin el .jpg, .png, .avi, .mpg

            if (auxmenu == 'v')
            {
                fileNameString = fileNameString + '_';

                // Add current frame to the name of the file
                stringstream stringCurrentFrame;
                stringCurrentFrame << fileNameString << current_frame;

                fileNameString = stringCurrentFrame.str();
            }

            // Color Option selected
            if (colorOption==true)
            {
                Mat colorsImg(height_image, width_image, CV_8UC3, Scalar(0,0,0));

                QColor Lbl_color;

                for (int i=0; i<num_labels; i++)
                {
                    num_Lbl_polygns=my_Labels[i].getNumPolygones();
                    Lbl_color=my_Labels[i].getLabelColor();

                    for (int j=0; j<num_Lbl_polygns; j++)
                    {
                        Lbl_polygon = *(my_Labels[i].getPolygon(j));

                        num_plg_vertexes = Lbl_polygon.getPolyNumVert();

                        Point poly_Vertexes[num_plg_vertexes];
                        for (int k=0; k<num_plg_vertexes; k++)
                            poly_Vertexes[k] = Lbl_polygon.getPolyVertex(k);

                        fillConvexPoly(colorsImg, poly_Vertexes, num_plg_vertexes,
                                       Scalar(Lbl_color.blue(), Lbl_color.green(), Lbl_color.red()));
                    }
                }

                // Save image
                string fileNameColor = fileNameString + "_colors.jpg";

                imwrite(fileNameColor, colorsImg);
            }

            // ID (graylevel) Option selected
            if (idOption==true)
            {
                Mat idImg(height_image, width_image, CV_8UC3, Scalar(0,0,0));

                int Lbl_ID;

                for (int i=0; i<num_labels; i++)
                {
                    num_Lbl_polygns=my_Labels[i].getNumPolygones();
                    Lbl_ID=my_Labels[i].getLabelID();

                    for (int j=0; j<num_Lbl_polygns; j++)
                    {
                        Lbl_polygon = *(my_Labels[i].getPolygon(j));
                        num_plg_vertexes = Lbl_polygon.getPolyNumVert();

                        Point poly_Vertexes[num_plg_vertexes];
                        for (int k=0; k<num_plg_vertexes; k++)
                            poly_Vertexes[k] = Lbl_polygon.getPolyVertex(k);

                        fillConvexPoly(idImg, poly_Vertexes, num_plg_vertexes, Scalar(Lbl_ID, Lbl_ID, Lbl_ID));
                    }
                }

                // Save image
                string fileNameID = fileNameString + "_IDs.jpg";

                imwrite(fileNameID, idImg);
            }

            // Instanciable Option selected
            if (instOption==true)
            {
                Mat instImg(height_image, width_image, CV_8UC3, Scalar(0,0,0));

                bool Lbl_inst;
                int Lbl_ID;

                for (int i=0; i<num_labels; i++)
                {
                    Lbl_inst=my_Labels[i].getLabelInstantiableState();
                    Lbl_ID=my_Labels[i].getLabelID();

                    if(Lbl_inst==true)
                    {
                        num_Lbl_polygns=my_Labels[i].getNumPolygones();

                        for (int j=0; j<num_Lbl_polygns; j++)
                        {
                            Lbl_polygon = *(my_Labels[i].getPolygon(j));
                            num_plg_vertexes = Lbl_polygon.getPolyNumVert();

                            Point poly_Vertexes[num_plg_vertexes];
                            for (int k=0; k<num_plg_vertexes; k++)
                                poly_Vertexes[k] = Lbl_polygon.getPolyVertex(k);

                            fillConvexPoly(instImg, poly_Vertexes, num_plg_vertexes, Scalar(Lbl_ID, Lbl_ID, Lbl_ID));
                        }
                    }
                }

                // Save image
                string fileNameInst = fileNameString + "_instanciables.jpg";

                imwrite(fileNameInst, instImg);
            }

            // Polygones Option selected
            if (polygonesOption==true)
            {
                // Save File Polygones Name
                string fileNamePoly = fileNameString + "_Polygones.xml";
                const char *fileNamePolyChar = fileNamePoly.c_str();

                // Create XML document
                XMLDocument xml_Polygones;

                // Create pointer to Root
                XMLNode *pRoot = xml_Polygones.NewElement("Root");
                xml_Polygones.InsertFirstChild(pRoot);

                // Variables
                QString nameLabel;
                string nameLabelStrg;
                const char *nameLabelChar;
                QColor colorLabel;
                int numLabels, numPolygns, numVertexes, IDLabel;
                bool instLabel, bboxLabel;
                Polygon polygon;
                Point vertex, topleftBBox;
                Rect boundingBox;
                Size sizeBBox;

                // Pointers Element
                XMLElement *pNumLabel, *pLabelElement, *pNameElement, *pIDElement, *pColorElement,
                        *pRedElement, *pGreenElement, *pBlueElement, *pInstElement, *pBBoxStateElement,
                        *pNumPolygones,*pPolygonElement, *pNumVertex, *pVertexElement, *pXElement, *pYElement,
                        *pBBoxElement, *pBBoxXElement, *pBBoxYElement, *pBBoxHeight, *pBBoxWidth;

                // Num of labels
                pNumLabel = xml_Polygones.NewElement("NumLabels");
                numLabels = my_Labels.size();
                pNumLabel->SetText(numLabels);
                pRoot->InsertEndChild(pNumLabel);

                for (int i=0; i<my_Labels.size(); i++)
                {
                    // Open Label
                    pLabelElement = xml_Polygones.NewElement("Label");

                    // Name
                    pNameElement = xml_Polygones.NewElement("Name");

                    nameLabel=my_Labels[i].getLabelName();
                    nameLabelStrg=nameLabel.toStdString();
                    nameLabelChar=nameLabelStrg.c_str();

                    pNameElement->SetText(nameLabelChar);
                    pLabelElement->InsertEndChild(pNameElement);

                    // ID
                    pIDElement = xml_Polygones.NewElement("ID");
                    IDLabel=my_Labels[i].getLabelID();
                    pIDElement->SetText(IDLabel);
                    pLabelElement->InsertEndChild(pIDElement);

                    // Color
                    pColorElement = xml_Polygones.NewElement("Color");
                    colorLabel=my_Labels[i].getLabelColor();

                    pRedElement = xml_Polygones.NewElement("Red");
                    pRedElement->SetText(colorLabel.red());
                    pColorElement->InsertEndChild(pRedElement);

                    pGreenElement = xml_Polygones.NewElement("Green");
                    pGreenElement->SetText(colorLabel.green());
                    pColorElement->InsertEndChild(pGreenElement);

                    pBlueElement = xml_Polygones.NewElement("Blue");
                    pBlueElement->SetText(colorLabel.blue());
                    pColorElement->InsertEndChild(pBlueElement);

                    pLabelElement->InsertEndChild(pColorElement);

                    // Instanciable State
                    pInstElement = xml_Polygones.NewElement("Instanciable");
                    instLabel=my_Labels[i].getLabelInstantiableState();
                    pInstElement->SetText(instLabel);
                    pLabelElement->InsertEndChild(pInstElement);

                    // Bounding Box State
                    pBBoxStateElement = xml_Polygones.NewElement("BoundingBox");
                    bboxLabel=my_Labels[i].getLabelBoundingBoxState();
                    pBBoxStateElement->SetText(bboxLabel);
                    pLabelElement->InsertEndChild(pBBoxStateElement);

                    // Num of Polygones
                    pNumPolygones = xml_Polygones.NewElement("NumPolygones");
                    numPolygns = my_Labels[i].getNumPolygones();
                    pNumPolygones->SetText(numPolygns);
                    pLabelElement->InsertEndChild(pNumPolygones);

                    for (int j=0; j<numPolygns; j++)
                    {
                        polygon = *(my_Labels[i].getPolygon(j));

                        // Open Polygon
                        pPolygonElement = xml_Polygones.NewElement("Polygon");

                        // Num of Vertexes
                        pNumVertex = xml_Polygones.NewElement("NumVertexes");
                        numVertexes = polygon.getPolyNumVert();
                        pNumVertex->SetText(numVertexes);
                        pPolygonElement->InsertEndChild(pNumVertex);

                        // Vertexes Position
                        for (int k=0; k<numVertexes; k++)
                        {
                            vertex = polygon.getPolyVertex(k);

                            // Open Vertex
                            pVertexElement = xml_Polygones.NewElement("Vertex");

                            // X coordinade
                            pXElement = xml_Polygones.NewElement("X");
                            pXElement->SetText(vertex.x);
                            pVertexElement->InsertEndChild(pXElement);

                            // Y coordinade
                            pYElement = xml_Polygones.NewElement("Y");
                            pYElement->SetText(vertex.y);
                            pVertexElement->InsertEndChild(pYElement);

                            // Close Vertex
                            pPolygonElement->InsertEndChild(pVertexElement);
                        }

                        // Bounding Box
                        if (bboxLabel==true)
                        {
                            boundingBox = boundingRect(polygon.getPolyVertexes());
                            topleftBBox = boundingBox.tl();
                            sizeBBox = boundingBox.size();

                            // OpenBoudingBox
                            pBBoxElement = xml_Polygones.NewElement("BoundingBoxDimensions");

                            // X coordinade top-left point
                            pBBoxXElement = xml_Polygones.NewElement("XBBox");
                            pBBoxXElement->SetText(topleftBBox.x);
                            pBBoxElement->InsertEndChild(pBBoxXElement);

                            // Y coordinade top-left point
                            pBBoxYElement = xml_Polygones.NewElement("YBBox");
                            pBBoxYElement->SetText(topleftBBox.y);
                            pBBoxElement->InsertEndChild(pBBoxYElement);

                            // Height of bounding box
                            pBBoxHeight = xml_Polygones.NewElement("HeightBBox");
                            pBBoxHeight->SetText(sizeBBox.height);
                            pBBoxElement->InsertEndChild(pBBoxHeight);

                            // Width of bounding box
                            pBBoxWidth = xml_Polygones.NewElement("WidthBBox");
                            pBBoxWidth->SetText(sizeBBox.width);
                            pBBoxElement->InsertEndChild(pBBoxWidth);

                            // Close Bounding Box
                            pPolygonElement->InsertEndChild(pBBoxElement);
                        }

                        //Close Polygon
                        pLabelElement->InsertEndChild(pPolygonElement);
                    }

                    // Close Label
                    pRoot->InsertEndChild(pLabelElement);
                }

                xml_Polygones.SaveFile(fileNamePolyChar, false);
            }

        // Current Frame selected
        if (currentFrame==true)
        {
            // Save frame
            string fileNameFrame = fileNameString + ".jpg";

            imwrite(fileNameFrame, frame);
        }


        newPolygnsSaved = true;

        if(videoOption)
        {
            i= image_Labels.size() -1;
            // If any option was selected
            if (colorOption || idOption || instOption || polygonesOption || currentFrame)
                QMessageBox::information(this, tr("Dataset exported"), tr("Selected Dataset exported successfully"));
        }

        if(i== image_Labels.size() -1)
        {
            // If any option was selected
            if (colorOption || idOption || instOption || polygonesOption || currentFrame)
                QMessageBox::information(this, tr("Dataset exported"), tr("Selected Dataset exported successfully"));
        }
      }
 }

}

///////////////////////////////////////

void MainWindow::exportLabelsAction()
{
    QString saveFileName = QFileDialog::getSaveFileName(this, tr("Save File"), QDir::currentPath(), tr("XML files (*.xml)"));

    string saveFileNameString = saveFileName.toStdString();
    saveFileNameString = saveFileNameString +".xml";
    const char *saveFileNameChar = saveFileNameString.c_str();

    // Create XML document
    XMLDocument xml_Labels;

    // Create pointer to Root
    XMLNode *pRoot = xml_Labels.NewElement("Root");
    xml_Labels.InsertFirstChild(pRoot);

    // Variables
    QString nameLabel;
    string nameLabelStrg;
    const char *nameLabelChar;
    QColor colorLabel;
    int numLabels, IDLabel;
    bool instLabel, bboxLabel;

    // Pointers Element
    XMLElement *pNumLabel, *pLabelElement, *pNameElement, *pIDElement, *pColorElement,
            *pRedElement, *pGreenElement, *pBlueElement, *pInstElement, *pBBoxElement;

    // Num of labels
    pNumLabel = xml_Labels.NewElement("NumLabels");
    numLabels = my_Labels.size();
    pNumLabel->SetText(numLabels);
    pRoot->InsertEndChild(pNumLabel);

    for (int i=0; i<numLabels; i++)
    {
        // Open Label
        pLabelElement = xml_Labels.NewElement("Label");

        // Name
        pNameElement = xml_Labels.NewElement("Name");

        nameLabel=my_Labels[i].getLabelName();
        nameLabelStrg=nameLabel.toStdString();
        nameLabelChar=nameLabelStrg.c_str();

        pNameElement->SetText(nameLabelChar);
        pLabelElement->InsertEndChild(pNameElement);

        // ID
        pIDElement = xml_Labels.NewElement("ID");
        IDLabel=my_Labels[i].getLabelID();
        pIDElement->SetText(IDLabel);
        pLabelElement->InsertEndChild(pIDElement);

        // Color
        pColorElement = xml_Labels.NewElement("Color");
        colorLabel=my_Labels[i].getLabelColor();

        pRedElement = xml_Labels.NewElement("Red");
        pRedElement->SetText(colorLabel.red());
        pColorElement->InsertEndChild(pRedElement);

        pGreenElement = xml_Labels.NewElement("Green");
        pGreenElement->SetText(colorLabel.green());
        pColorElement->InsertEndChild(pGreenElement);

        pBlueElement = xml_Labels.NewElement("Blue");
        pBlueElement->SetText(colorLabel.blue());
        pColorElement->InsertEndChild(pBlueElement);

        pLabelElement->InsertEndChild(pColorElement);

        // Instanciable State
        pInstElement = xml_Labels.NewElement("Instanciable");
        instLabel=my_Labels[i].getLabelInstantiableState();
        pInstElement->SetText(instLabel);
        pLabelElement->InsertEndChild(pInstElement);

        // Bounding Box State
        pBBoxElement = xml_Labels.NewElement("BoundingBox");
        bboxLabel=my_Labels[i].getLabelBoundingBoxState();
        pBBoxElement->SetText(bboxLabel);
        pLabelElement->InsertEndChild(pBBoxElement);

        // Close Label
        pRoot->InsertEndChild(pLabelElement);
    }

    xml_Labels.SaveFile(saveFileNameChar, false);
    QMessageBox::information(this, tr("Labels exported"), tr("Labels Set exported successfully"));
}

///////////////////////////////////////
void MainWindow::on_exportLabelPointsCloud_triggered()
//void MainWindow::exportLabelPointsCloudAction()
{
    //Saves the pointcloud
    QString aux = "/pointCloud";
    int index = image_index +1;
    aux = aux.append(index);
    aux = aux.append(".pcd");
    imagesName = imagesName.append(aux);
    pcl::io::savePCDFile(imagesName.toStdString(), my_lidarInformation3D, true); //Binary format
}

///////////////////////////////////////


void MainWindow::on_zoomIn_clicked()
{
    zoom(1.25);
    zoomActivated=true;

    for(int i=0; i<my_Labels.size(); i++)
    {
          my_Labels[i].modificatedVertex(appliedFactor);
          my_Labels[i].modificatedPolygon(appliedFactor);

          //Change the value of bounding box created in the interpolation
          vector<Polygon*> polyAux = my_Labels[i].getPolygones();
          for(int p=0; p<polyAux.size(); p++)
          {
              if(my_Labels[i].getLabelBoundingBoxState() /*&& polyAux[p]->getPolyVertexes().empty()*/)
              {
                  polyAux[p]->modificatedBBox(appliedFactor);
                  Instantiable* instan = (Instantiable*) polyAux[p];
                  instan->modificatedBBoxI(appliedFactor);
              }
          }
    }

    accumulatedFactor=accumulatedFactor*appliedFactor;

     paint();

}

///////////////////////////////////////

void MainWindow::on_zoomOut_clicked()
{
    zoom(0.8);
    zoomActivated=true;

    for(int i=0; i<my_Labels.size(); i++)
    {
          my_Labels[i].modificatedVertex(appliedFactor);
          my_Labels[i].modificatedPolygon(appliedFactor);

          //Change the value of bounding box created in the interpolation
          vector<Polygon*> polyAux = my_Labels[i].getPolygones();
          for(int p=0; p<polyAux.size(); p++)
          {
              if(my_Labels[i].getLabelBoundingBoxState())
              {
                  polyAux[p]->modificatedBBox(appliedFactor);
                  Instantiable* instan = (Instantiable*) polyAux[p];
                  instan->modificatedBBoxI(appliedFactor);
              }
          }
    }

    accumulatedFactor=accumulatedFactor*appliedFactor;

     paint();

}

///////////////////////////////////////

void MainWindow::on_normalSizeAct_clicked()
{
    zoomActivated=false;

    normalSize();
}

///////////////////////////////////////

void MainWindow::on_listWidget_clicked(const QModelIndex &index)
{
     index_label=index.row();

    if (index.row()<0)
    {
        QMessageBox::warning(this, tr("Select Label"), tr("Please, select a Label"));
        return;
    }
    else{
        if(index.row()!=-1 && my_Labels[index_label].getNumVertexes()>0)
        {
            QMessageBox newDecision;
            newDecision.setWindowTitle("Change label");
            newDecision.setText("Do you want to change label?");
            newDecision.setInformativeText("If you change, you'll lose current Vertexes. "
                                           "You can click 'No' and create a polygon, to save Vertexes.");
            newDecision.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            newDecision.setDefaultButton(QMessageBox::No);
            newDecision.setIcon(QMessageBox::Question);
            int ret = newDecision.exec();

            if (ret == QMessageBox::No)
                return;

            else if (ret == QMessageBox::Yes)
            {
                int vertexes = my_Labels[index_label].getNumVertexes();

                // Delete all vertexes
                for (int i=0; i<vertexes; i++)
                    my_Labels[index_label].deleteLastVertex();

                paint();
            }
        }
        labelSelected=true;
    }
}

///////////////////////////////////////

void MainWindow::on_listWidget_doubleClicked(const QModelIndex &index)
{

    labelSelected=true;
    index_label=index.row();

    viewlabel selectedLabel;
    selectedLabel.setWindowTitle("Selected Label");
    selectedLabel.setLabel(my_Labels[index_label]);
    selectedLabel.exec();

}

///////////////////////////////////////

void MainWindow::on_deletePolygon_clicked()
{
        int index=ui->tablePolygones->currentRow();

        if (index<0)
        {
            QMessageBox::warning(this, tr("Delete Polygon"), tr("Please, select a Polygon to delete"));
            return;
        }

        if(!polygonesLabel[index]->getPolyName().contains(my_Labels[index_label].getLabelName()))
           {

            QMessageBox::warning(this, tr("Delete Polygon"), tr("Please, selecte the correct label to delete the polygon"));
            return;
           }

        //Searchs the polygon selected to remove it
        int positionPolygonDeleted;
        int numpolygonesLabel=my_Labels[index_label].getNumPolygones();

        for(int i=0; i<numpolygonesLabel; i++)
           {
               if(polygonesLabel[index]->getPolyName()==my_Labels[index_label].getPolygonName(i))
               {
                  positionPolygonDeleted=i;
                   my_Labels[index_label].deletePolygon(i);
                   ui->tablePolygones->removeRow(index);
               }
           }

        if(!my_lidarInformation2D.empty())
        {
            for(int i=0; i<numpolygonesLabel; i++)
            {
                if(polygonesLabel[index]->getPolyName()==my_Labels[index_label].getPolygonName(i))
                {
                   if(!my_lidarInformation2D.empty() && my_Labels[index_label].getPolyID(i)==my_lidarInformation2D[i].getLabelID())
                   {
                       my_lidarInformation2D.erase(my_lidarInformation2D.begin()+i);
                   }
                }
            }
        }

        for(int i=0; i<numpolygonesLabel; i++)
        {
            if(polygonesLabel[index]->getPolyName()==my_Labels[index_label].getPolygonName(i))
            {
                positionPolygonDeleted=i;
                my_Labels[index_label].deletePolygon(i);
                ui->tablePolygones->removeRow(index);
                my_Labels[index_label].deletePolygonF(i);
            }
        }



        paint();

        polygonesLabel.erase(polygonesLabel.begin()+index);

        //Updates the numbers of the polygons
        for(int i=0; i<polygonesLabel.size(); i++)
        {
          if(polygonesLabel.size()>0 && (polygonesLabel[i]->getPolyName()==my_Labels[index_label].getPolygonName(positionPolygonDeleted)))
          {
                 my_Labels[index_label].setPolygonName(positionPolygonDeleted);
                 polygonesLabel[i]->setPolyName(positionPolygonDeleted+1, my_Labels[index_label].getLabelName());
                 positionPolygonDeleted++;
           }
        }

        //Clear and include the  polygons again
        for(int r=0; r<=ui->tablePolygones->rowCount(); r++)
        {
            ui->tablePolygones->takeItem(r,0);
            ui->tablePolygones->takeItem(r,1);
        }
        int indexAux=0;
        while(indexAux <= index_label)
        {
            for(int i=0; i<polygonesLabel.size(); i++)
            {
               if(my_Labels[indexAux].getLabelInstantiableState())
               {
                   Instantiable* instan = (Instantiable*) polygonesLabel[i];
                   ui->tablePolygones->setItem(i,0,new QTableWidgetItem(instan->getPolyName()));
                   ui->tablePolygones->setItem(i,1,new QTableWidgetItem(QString::number(instan->getPolyID())));
               }
               else
               {
                    NoInstantiable *noInstan = (NoInstantiable*) polygonesLabel[i];
                    ui->tablePolygones->setItem(i,0,new QTableWidgetItem(noInstan->getPolyName()));
               }
             }
           indexAux++;
        }
}

///////////////////////////////////////

void MainWindow::on_tablePolygones_doubleClicked(const QModelIndex &index)
{
    if(index.column()==0)
        ui->tablePolygones->setEditTriggers(QAbstractItemView :: NoEditTriggers );

    else if(index.column()==1)
    {
          //Edit the value of polygon's ID
          ui->tablePolygones->edit(index);
          polygonEdit=true;
    }
}

///////////////////////////////////////

void MainWindow::on_tablePolygones_cellChanged(int row, int column)
{
    QTableWidgetItem *newIDWidget, *newNameFoundWidget;
    int newID;
    QString nameFound;

  if(polygonEdit==true && column == 1)
   {
    newIDWidget=ui->tablePolygones->item(row, column);
    newID=newIDWidget->text().toInt();

    newNameFoundWidget=ui->tablePolygones->item(row, column-1);
    nameFound=newNameFoundWidget->text();

    //Search the polygon's name associated
    for(int i=0; i<polygonesLabel.size(); i++)
    {
      //Change the value polygon's ID of the vectors
        Instantiable *instan = (Instantiable*) polygonesLabel[row];
        instan->setPolyID(newID);
    }

    for(int i=0; i<my_Labels.size(); i++)
    {
       if(nameFound.contains(my_Labels[i].getLabelName()))
       {
           for(int j=0; j<my_Labels[i].getPolygones().size(); j++)
           {
               if((nameFound.compare(nameFound,my_Labels[i].getPolygones()[j]->getPolyName(),Qt::CaseInsensitive))==0)
               {
                   Instantiable *instan = (Instantiable*) my_Labels[i].getPolygones()[j];
                   instan->setPolyID(newID);
                }
           }

       }
    }

    }
  polygonEdit=false;
}

///////////////////////////////////////

void MainWindow::on_tablePolygones_clicked(const QModelIndex &index)
{
    indexPolygon=index.row();
    paint();

    Mat imgDrawn;
    imgDrawn = cvImg.clone();

    // Draw all existing polygones
    QColor label_color;
    int num_polygones_label;
    Polygon* label_polygon;
    int num_vertexes_polygon;

    for (int i=0; i<my_Labels.size(); i++)
    {
        num_polygones_label=my_Labels[i].getNumPolygones();
        label_color=my_Labels[i].getLabelColor();

        for (int j=0; j<num_polygones_label; j++)
        {
            label_polygon = my_Labels[i].getPolygon(j);

            if(my_Labels[i].getLabelBoundingBoxState() && label_polygon->getPolyVertexes().empty())
            {
                Instantiable* instan = (Instantiable*) label_polygon;
                Rect BBox(instan->getBoundingBox().x(), instan->getBoundingBox().y(),instan->getBoundingBox().width(),
                          instan->getBoundingBox().height());
                rectangle(imgDrawn, BBox, Scalar(0,0,0), 2);
            }

            else if(my_Labels[i].getLabelBoundingBoxState() && !label_polygon->getPolyVertexes().empty())
            {
                num_vertexes_polygon = label_polygon->getPolyNumVert();
                int *pNumVertex = &num_vertexes_polygon;

                Point poly_Vertexes[num_vertexes_polygon];
                for (int k=0; k<num_vertexes_polygon; k++){
                     poly_Vertexes[k] = label_polygon->getPolyVertex(k);
                }
                const Point *pVertex = poly_Vertexes;
                const Point **pPointer = &pVertex;

                fillPoly(imgDrawn, pPointer, pNumVertex, 1,
                               Scalar(label_color.blue(), label_color.green(), label_color.red()));

                Instantiable* instan = (Instantiable*) label_polygon;
                Rect BBox(instan->getBoundingBox().x(), instan->getBoundingBox().y(),instan->getBoundingBox().width(),
                          instan->getBoundingBox().height());
                rectangle(imgDrawn, BBox, Scalar(0,0,0), 2);
            }

            else
            {
                num_vertexes_polygon = label_polygon->getPolyNumVert();
                int *pNumVertex = &num_vertexes_polygon;

                Point poly_Vertexes[num_vertexes_polygon];
                for (int k=0; k<num_vertexes_polygon; k++){
                     poly_Vertexes[k] = label_polygon->getPolyVertex(k);
                }
                const Point *pVertex = poly_Vertexes;
                const Point **pPointer = &pVertex;

                fillPoly(imgDrawn, pPointer, pNumVertex, 1,
                               Scalar(label_color.blue(), label_color.green(), label_color.red()));
            }

            //Marks with another color the polygon selected
            Polygon* poly = polygonesLabel[indexPolygon];

            if(label_polygon->getPolyName()==poly->getPolyName())
            {
                Instantiable* instan = (Instantiable*) label_polygon;
                Rect BBox(instan->getBoundingBox().x(), instan->getBoundingBox().y(),instan->getBoundingBox().width(),
                          instan->getBoundingBox().height());
                rectangle(imgDrawn, BBox, CV_RGB(247,148,43), 2);
            }
        }
    }

    if(index_label>=0)
    {

        // Draw a circle to mark Vertexes and a line to connect them
        QColor color = my_Labels[index_label].getLabelColor();
        int numVertexes = my_Labels[index_label].getNumVertexes();

        for (int i=0; i<numVertexes; i++)
        {
            circle(imgDrawn, my_Labels[index_label].getVertex(i), 3, Scalar(0, 0, 0)); // Black edge
            circle(imgDrawn, my_Labels[index_label].getVertex(i), 2, Scalar(color.blue(), color.green(), color.red()), -1 ); // BGR format

            // Draw Lines if there are more than one vertex
            if(i>0)
            {

                line(imgDrawn, my_Labels[index_label].getVertex(i-1), my_Labels[index_label].getVertex(i), Scalar(0, 0, 0), 4);
                line(imgDrawn, my_Labels[index_label].getVertex(i-1), my_Labels[index_label].getVertex(i),
                     Scalar(color.blue(), color.green(), color.red()), 2);
            }
        }
    }

    // Show image in QLabel
    QImage vertexImage=QImage(imgDrawn.data, imgDrawn.cols, imgDrawn.rows, imgDrawn.step, QImage::Format_RGB888);
    QImage vertexImageRGB=vertexImage.rgbSwapped();

    if(zoomActivated){
        ui->labelImage->setPixmap(QPixmap::fromImage(vertexImageRGB));
        ui->labelImage->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    }
    else{
        ui->labelImage->setPixmap(QPixmap::fromImage(vertexImageRGB));
        ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);
   }

    label_image->show();

    imgDrawn.release();

}

///////////////////////////////////////

void MainWindow::on_Interpolate_clicked(bool checked)
{
    getInterval();

    int imgInterpolate = image_index - interval;
    int limitInterpolate = image_index;
    QString labelNameInter;
    int indexLabel;

    QString namePolySelected= polygonesLabel[indexPolygon]->getPolyName();

    //Searches the label's name in current image
    for(int i=0; i<my_Labels.size(); i++)
    {
        vector<Polygon*> polyAux = my_Labels[i].getPolygones();
        for(int p=0; p<polyAux.size(); p++)
        {
            if(polyAux[p]->getPolyName().contains(namePolySelected))
            {
                labelNameInter=my_Labels[i].getLabelName();
                indexLabel = i;
            }
        }
    }

    if(num_image==1)
    {
        QMessageBox::warning(this, tr("Interpolation"), tr("Option disabled. There is only one image."));
        return;
    }

    else if(!videoOption)
    {
        QMessageBox::warning(this, tr("Interpolation"), tr("Option disabled."));
        return;
    }

    else if(my_Labels[indexLabel].getLabelInstantiableState() && my_Labels[indexLabel].getLabelBoundingBoxState())
    {
        //Found polygon selected to interpolation in current image
        for(int i=0; i<my_Labels.size(); i++)
        {
            vector<Polygon*> polyAux = my_Labels[i].getPolygones();
            for(int p=0; p<polyAux.size(); p++)
            {
                if(polyAux[p]->getPolyName().contains(namePolySelected))
                  polySelectedLast = (Instantiable*) polyAux[p];
            }
        }

        int polyIDSelected = polySelectedLast->getPolyID();
        bool polyFound=false;

        QMessageBox newDecision;
        newDecision.setWindowTitle("Interpolation");
        newDecision.setText("You have selected "+namePolySelected+ " in image "+ QString::number(image_index+1)+ ". Do you want to use this polygon to interpolate?");
        newDecision.setInformativeText("If you don't want this polygon, please selectes other polygon");
        newDecision.setStandardButtons(QMessageBox::No | QMessageBox::Yes);
        newDecision.setDefaultButton(QMessageBox::Yes);
        newDecision.setIcon(QMessageBox::Question);
        int ret = newDecision.exec();

        if (ret == QMessageBox::No)
            return;

        else if (ret == QMessageBox::Yes)
        {
           for(int i=0; i<image_Labels[imgInterpolate].size(); i++)
           {
              vector<Polygon*> polygonAux=image_Labels[imgInterpolate][i].getPolygones();

              for(int p=0; p<polygonAux.size(); p++)
              {
                 polySelectedFirst = (Instantiable*) polygonAux[p];
                 //Searches the polygon's ID in the image
                 if(polySelectedFirst->getPolyID()==polyIDSelected)
                 {
                    QRectF BBoxPolySelectedFirst = polySelectedFirst->getBoundingBox();
                    QRectF BBoxPolySelectedLast = polySelectedLast->getBoundingBox();

                    //Calculates the distance between the bounding boxes
                    topLeftD= BBoxPolySelectedLast.topLeft() - BBoxPolySelectedFirst.topLeft();
                    topRightD = BBoxPolySelectedLast.topRight() - BBoxPolySelectedFirst.topRight();
                    bottonLeftD = BBoxPolySelectedLast.bottomLeft() - BBoxPolySelectedFirst.bottomLeft();
                    bottonRightD = BBoxPolySelectedLast.bottomRight() - BBoxPolySelectedFirst.bottomRight();

                    polyFound=true;
                 }

              }
           }
           if(!polyFound)
           {
               QMessageBox::warning(this, tr("Interpolation"), tr("Polygon not found. Revises that the polygon's ID is correct."));
               return;
           }

           //Intializes the counter
              contInterval = 0;

           while(imgInterpolate<limitInterpolate)
           {
               for(int i=0; i<image_Labels[imgInterpolate+1].size(); i++)
               {
                  if(image_Labels[imgInterpolate+1][i].getLabelName().contains(labelNameInter))
                  {
                      //Interpolation
                      Instantiable* polyInterpolation = new Instantiable(polySelectedFirst);
                      QPointF tld= topLeftD;
                      tld*=(double)(contInterval+1)/interval;
                      QPointF topLeftAux(polySelectedFirst->getBoundingBox().topLeft().x() + (double)(contInterval+1)/interval*topLeftD.x(),
                                         polySelectedFirst->getBoundingBox().topLeft().y() + (double)(contInterval+1)/interval*topLeftD.y()); //= (polySelectedFirst->getBoundingBox().topLeft() + tld);
                      QPointF bottonRightAux = (polySelectedFirst->getBoundingBox().bottomRight() + (double)(contInterval+1)/interval*bottonRightD);

                       polyInterpolation->deleteVertexes();
                       polyInterpolation->setBoundingBoxInterpolation(topLeftAux, bottonRightAux);
                       polyInterpolation->setBoundingBox(topLeftAux, bottonRightAux);

                       //Add polygon in the label
                       image_Labels[imgInterpolate+1][i].setPolygon(polyInterpolation);
                  }
               }
               imgInterpolate++;
               contInterval++;
           }
           paint();
        }
    }

    else
    {
        QMessageBox::warning(this, tr("Interpolation"), tr("The interpolation must have checked the buttons of instantiable and bounding box."));
        return;
    }

}

///////////////////////////////////////

void MainWindow::on_showLidarButton_clicked(bool checked)
{
    lidar_checked=checked;
   /* if(mode2DButton->isEnabled())
        mode2DButton->click();
    if(mode3DButton->isEnabled())
        mode3DButton->click();*/

    Mat imageMat(imageFileOriginal.height(), imageFileOriginal.width(), CV_8UC3, Scalar(0, 0, 0));  //CV_8C3

    //If Lidar button is selected
    if(lidar_checked)
    {
       // setStyleSheet("QPushButton:checked { background-color: orange; border-style: double; border-width: 7px; border-radius: 10px;}");

        //Hide the elements of 2D mode
        ui->listWidget->hide();
        ui->Labels_label->hide();
        ui->addButton->hide();
        ui->editButton->hide();
        ui->deleteButton->hide();

        ui->label->hide();
        ui->tablePolygones->hide();
        ui->deletePolygon->hide();
        ui->normalSizeAct->hide();
        ui->zoomIn->hide();
        ui->zoomOut->hide();


        Lidar lidar;
        cv::resize(lidar.showCalibrationMatrix(imgFile, infoImages[image_index].getFullMatrix(), pathLidar,pathCalib, auxNameFile, infoImages[image_index].getMatrix(), lidar_checked), imageMat,
                   Size(imageFileOriginal.width(), imageFileOriginal.height()));
        fullMatrix=lidar.getFullMatrix();
        matrixVeloToImg=lidar.getMatrix();

        //Convert BGR to RGB
        cv::cvtColor(imageMat, imageMat, cv::COLOR_BGR2RGB);

        QImage imageAux((uchar*)imageMat.data, imageMat.cols, imageMat.rows, imageMat.step, QImage::Format_RGB888);

        imageFile=imageAux;

         // Show image
         ui->labelImage->setPixmap(QPixmap::fromImage(imageFile));
         ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

         label_image->show();

         //Save vector 2D and vector 3D
         point2D = lidar.saveVector2d();
         point3D = lidar.saveVector3d();

         //include info in the vector to relationate the pointcloud to labels
         infoImages[image_index].setPoint2D(point2D);
         infoImages[image_index].setPoint3D(point3D);
         infoImages[image_index].setMatrix(matrixVeloToImg);
    }

    else
    {
        //Show the elements of 2D mode
        ui->listWidget->show();
        ui->Labels_label->show();
        ui->addButton->show();
        ui->editButton->show();
        ui->deleteButton->show();

        ui->label->show();
        ui->tablePolygones->show();
        ui->deletePolygon->show();
        ui->normalSizeAct->show();
        ui->zoomIn->show();
        ui->zoomOut->show();

        Mat imgDrawn (imageFileOriginal.height(), imageFileOriginal.width(), CV_8UC3, Scalar(0, 0, 0));
        cvImg.copyTo(imgDrawn);

        //Convert BGR to RGB
        QImage vertexImage=QImage(imgDrawn.data, imgDrawn.cols, imgDrawn.rows, imgDrawn.step, QImage::Format_RGB888);
        QImage vertexImageRGB=vertexImage.rgbSwapped();

        imageFile=vertexImageRGB;

         // Show image
         ui->labelImage->setPixmap(QPixmap::fromImage(imageFile));
         ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

         label_image->show();
               //   paint();
    }
}

///////////////////////////////////////

void MainWindow::on_mode3D_clicked(bool checked)
{
    /*if(mode2DButton->isEnabled())
        mode2DButton->click();
    if(lidarButton->isEnabled())
        lidarButton->click();*/
    mode3D_check = checked;


    //If Lidar button is selected
    if(mode3D_check && !my_Labels.empty())
    {
          Mat imageMat(imageFileOriginal.height(), imageFileOriginal.width(), CV_8UC3, Scalar(0, 0, 0));  //CV_8C3
          setStyleSheet("QPushButton:checked { background-color: orange; border-style: double; border-width: 7px; border-radius: 10px;}");

          //Hide the elements of 2D mode
          ui->listWidget->hide();
          ui->Labels_label->hide();
          ui->addButton->hide();
          ui->editButton->hide();
          ui->deleteButton->hide();

          ui->label->hide();
          ui->tablePolygones->hide();
          ui->deletePolygon->hide();
          ui->normalSizeAct->hide();
          ui->zoomIn->hide();
          ui->zoomOut->hide();

            Lidar lid;
           // imageMat=lid.showCalibrationMatrix(imgFile, fullMatrix, pathLidar,pathCalib, auxNameFile, matrixVeloToImg);
           cv::resize(lid.showCalibrationMatrix(imgFile, fullMatrix, pathLidar,pathCalib, auxNameFile, matrixVeloToImg, lidar_checked), imageMat,
                       Size(imageFileOriginal.width(), imageFileOriginal.height()));
            fullMatrix=lid.getFullMatrix();
            matrixVeloToImg=lid.getMatrix();

             //Save vector 2D and vector 3D
             point2D = lid.saveVector2d();
             point3D = lid.saveVector3d();

             //include info in the vector to relationate the pointcloud to labels
             infoImages[image_index].setPoint2D(point2D);
             infoImages[image_index].setPoint3D(point3D);
             infoImages[image_index].setFullMatrix(fullMatrix);
             infoImages[image_index].setMatrix(matrixVeloToImg);

             //Look if pointcloud is include into the some label
             bool pointIncluded;
            for(int i=0; i<point2D.size(); i++)
            {
                for(int j=0; j<my_Labels.size(); j++)
                {
                    QVector <QPolygonF> my_PolygonesF = my_Labels[j].getPolygonesF();
                    for(int p=0; p<my_PolygonesF.size(); p++)
                    {
                        QPointF pointAux(point2D[i].x,point2D[i].y);
                        pointIncluded = my_PolygonesF[p].containsPoint(pointAux, Qt::WindingFill);
                        lidarinformation lidarMyVector;

                        if(pointIncluded)
                        {
                            int id = my_Labels[j].getLabelID();
                            lidarMyVector.setLabelID(id);
                            QColor color = my_Labels[j].getLabelColor();
                            lidarMyVector.setLabelColor(color);
                            lidarMyVector.setPoint(pointAux);

                            my_lidarInformation2D.push_back(lidarMyVector);

                            //Save 3D information
                            pcl::PointXYZRGB point;
                            point.x = point3D[i].x ;
                            point.y = point3D[i].y;
                            point.z = point3D[i].z;
                            point.r = (uint8_t) color.red();
                            point.g = (uint8_t) color.green();
                            point.b = (uint8_t) color.blue();

                            my_lidarInformation3D.push_back(point);

                        }
                    }
                }
              }

            //Include my_lidarInformation into the infoImages vector
             infoImages[image_index].setVectorLidar(my_lidarInformation2D);

            //Paint point cloud with label's color
            // Mat imgDrawn (imageFileOriginal.height(), imageFileOriginal.width(), CV_8UC3, Scalar(0, 0, 0));
             //cvImg.copyTo(imgDrawn);
             Mat imgDrawn = imgFile;

            int numVertexes = my_lidarInformation2D.size();
            int cont =0;

            for (int i=0; i<numVertexes; i++)
            {
                 QColor color = my_lidarInformation2D[i].getLabelColor();
                 circle(imgDrawn, my_lidarInformation2D[i].getPoint(), 3, Scalar(0, 0, 0)); // Black edge
                 circle(imgDrawn, my_lidarInformation2D[i].getPoint(), 2, Scalar(color.blue(), color.green(), color.red()), -1 ); // BGR format
                cont++;
            }

            cv::resize(imgDrawn, imageMat,
                       Size(imageFileOriginal.width(), imageFileOriginal.height()));

            //Convert BGR to RGB
           // QImage vertexImage=QImage(imgDrawn.data, imgDrawn.cols, imgDrawn.rows, imgDrawn.step, QImage::Format_RGB888);
           // QImage vertexImageRGB=vertexImage.rgbSwapped();
            cv::cvtColor(imageMat, imageMat, cv::COLOR_BGR2RGB);

            QImage imageAux((uchar*)imageMat.data, imageMat.cols, imageMat.rows, imageMat.step, QImage::Format_RGB888);

            imageFile=imageAux;

             // Show image
             ui->labelImage->setPixmap(QPixmap::fromImage(imageFile));
             ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

             label_image->show();
    }

    else if(!mode3D_check && !my_Labels.empty())
    {
        //Show the elements of 2D mode
        ui->listWidget->show();
        ui->Labels_label->show();
        ui->addButton->show();
        ui->editButton->show();
        ui->deleteButton->show();

        ui->label->show();
        ui->tablePolygones->show();
        ui->deletePolygon->show();
        ui->normalSizeAct->show();
        ui->zoomIn->show();
        ui->zoomOut->show();

        Mat imgDrawn (imageFileOriginal.height(), imageFileOriginal.width(), CV_8UC3, Scalar(0, 0, 0));
        cvImg.copyTo(imgDrawn);

        //Convert BGR to RGB
        QImage vertexImage=QImage(imgDrawn.data, imgDrawn.cols, imgDrawn.rows, imgDrawn.step, QImage::Format_RGB888);
        QImage vertexImageRGB=vertexImage.rgbSwapped();

        imageFile=vertexImageRGB;

         // Show image
         ui->labelImage->setPixmap(QPixmap::fromImage(imageFile));
         ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

         label_image->show();
               //   paint();
    }

    else if(my_Labels.empty())
    {
        QMessageBox::warning(this, tr("Mode 3D"), tr("This mode is disenabled, you must create some label."));
        return;
    }
}

///////////////////////////////////////

void MainWindow::on_mode2D_clicked(bool checked)
{
    mode2D_check = checked;
  /*  if(lidarButton->isEnabled())
        lidarButton->click();
    if(mode3DButton->isEnabled())
        mode3DButton->click();*/

    //Show the elements of 2D mode
    ui->listWidget->show();
    ui->Labels_label->show();
    ui->addButton->show();
    ui->editButton->show();
    ui->deleteButton->show();

    ui->label->show();
    ui->tablePolygones->show();
    ui->deletePolygon->show();
    ui->normalSizeAct->show();
    ui->zoomIn->show();
    ui->zoomOut->show();


    Mat imgDrawn (imageFileOriginal.height(), imageFileOriginal.width(), CV_8UC3, Scalar(0, 0, 0));
    cvImg.copyTo(imgDrawn);

    //Convert BGR to RGB
    QImage vertexImage=QImage(imgDrawn.data, imgDrawn.cols, imgDrawn.rows, imgDrawn.step, QImage::Format_RGB888);
    QImage vertexImageRGB=vertexImage.rgbSwapped();

    imageFile=vertexImageRGB;

     // Show image
     ui->labelImage->setPixmap(QPixmap::fromImage(imageFile));
     ui->labelImage->setAlignment(Qt::AlignCenter | Qt::AlignCenter);

     label_image->show();
              paint();

}

///////////////////////////////////////

double MainWindow::modificatedFactorX()
{
    factorX = (double) imgFileOriginal.cols/width_imageOriginal;

    return factorX;
}

///////////////////////////////////////

double MainWindow::modificatedFactorY()
{
   factorY = (double) imgFileOriginal.rows/height_imageOriginal;

   return factorY;
}

///////////////////////////////////////

