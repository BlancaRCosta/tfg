#include "labeldialog.h"
#include "ui_labeldialog.h"

///////////////////////////////////////

LabelDialog::LabelDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LabelDialog)
{
    ui->setupUi(this);
    colorWidget=ui->colorWidget;
    colorSelected = false;
    findID=false;
    findName=false;
}

///////////////////////////////////////

LabelDialog::~LabelDialog()
{
    delete ui;
}

///////////////////////////////////////

void LabelDialog::on_chooseColorButton_clicked()
{
    color = QColorDialog::getColor(Qt::white, this);

    QPalette pal = colorWidget -> palette();
    pal.setColor(QPalette::Window, color);
    colorWidget->setPalette(pal);
    colorSelected = true;
}

///////////////////////////////////////

void LabelDialog::on_saveLabelButton_clicked()
{
    bool check=true;

    QString text = ui-> lineName->text();
    QString id_text = ui->lineID->text();
     bool ok;
     int id=id_text.toInt(&ok,10);

    if(mode==0)
    {
        for(int i=0; i<saveLabels.size(); i++)
         {
           if(saveLabels[i].getLabelID()==id)
              findID=true;

           if(saveLabels[i].getLabelName()==text)
              findName=true;
         }
    }

     else if(mode==1)
     {
       for(int i=0; i<saveLabels.size(); i++)
        {
           if(i!= indexLabel)
           {
             if(saveLabels[i].getLabelID()==id)
               findID=true;

              if(saveLabels[i].getLabelName()==text)
               findName=true;
            }

          }
      }

    //Name

    if (text.isEmpty())
    {
        QMessageBox::warning(this, tr("Label name"), tr("Please, write a name for the label"));
        check=false;
    }

    else if(findName)
    {
        QMessageBox::warning(this, tr("Label name"), tr("The name inserted is caught. Please, choose another name."));
        check=false;
        findName=false;
    }

    else
        mynewlabel.setLabelName(text);

    //ID

    if (id_text.isEmpty())
    {
        QMessageBox::warning(this, tr("Label ID"), tr("Please, write a ID for the label"));
        check=false;
    }

    else{

        if (id<0 || id>255)
        {
            QMessageBox::warning(this, tr("Label ID"), tr("Please, ID label must be between 0 and 255"));
            check=false;
        }

        else if(findID)
       {
           QMessageBox::warning(this, tr("Label ID"), tr("The ID inserted is caught. Please, choose another value."));
           check=false;
           findID=false;
       }
        else
            mynewlabel.setLabelID(id);

    }

    //Color
    if(colorSelected==false && mode==0)
    {
        QMessageBox::critical(this, tr("Label color"), tr("Please, select a color for the label"));
        check=false;
    }
    else if(color.red()<=10 && color.blue()<=10 && color.green()<=10)
    {
        QMessageBox::critical(this, tr("Label color"), tr("Please, select a color other than black"));
        check=false;
    }
    else
        mynewlabel.setLabelColor(color);


    //Instanciable State
    bool instanciable = ui->instanciable_box->isChecked();
    mynewlabel.setLabelInstantiableState(instanciable);

    //Bounding Box State
    bool bbox = ui->BB_box->isChecked();
    mynewlabel.setLabelBoundingBoxState(bbox);

    if(bbox==true && instanciable==false)
      {
        QMessageBox::warning(this, tr("Label Bounding Box"), tr("You can't check bounding box if not check instantiable. Please, check instantiable too."));
        check=false;
    }

    if(check==true) // Everything OK
        close();
}

///////////////////////////////////////

Label LabelDialog::getMyLabel()
{
    return mynewlabel;
}

///////////////////////////////////////

void LabelDialog::setMyLabels(vector<Label> labels)
{
   // saveLabels.clear();
    saveLabels=labels;
}

///////////////////////////////////////

void LabelDialog::setIndexLabel(int index)
{
    indexLabel=index;
}

///////////////////////////////////////

void LabelDialog::setLabelAttributes(Label current_label)
{

    QString currentLabel_name = current_label.getLabelName();
    ui-> lineName->setText(currentLabel_name);

    int currentLabel_ID = current_label.getLabelID();
    QString currentLabel_IDString = QString::number(currentLabel_ID,10);
    ui -> lineID -> setText(currentLabel_IDString);

    QColor currentLabel_color = current_label.getLabelColor();
    color = currentLabel_color;
    QPalette currentLabel_pal = colorWidget -> palette();
    currentLabel_pal.setColor(QPalette::Window, currentLabel_color);
    colorWidget->setPalette(currentLabel_pal);

    bool currentLabel_inst = current_label.getLabelInstantiableState();
    ui->instanciable_box->setChecked(currentLabel_inst);

    bool currentLabel_bbox = current_label.getLabelBoundingBoxState();
    ui->BB_box->setChecked(currentLabel_bbox);

}

///////////////////////////////////////

void LabelDialog::setMode(int selectedMode)
{
    mode = selectedMode;
}

///////////////////////////////////////

