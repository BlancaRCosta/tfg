#include "imageviewer.h"

class ImageViewerData : public QSharedData
{
public:

};

ImageViewer::ImageViewer() : data(new ImageViewerData)
{

}

ImageViewer::ImageViewer(const ImageViewer &rhs) : data(rhs.data)
{

}

ImageViewer &ImageViewer::operator=(const ImageViewer &rhs)
{
    if (this != &rhs)
        data.operator=(rhs.data);
    return *this;
}

ImageViewer::~ImageViewer()
{

}
