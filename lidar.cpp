#include "lidar.h"

Lidar::Lidar()
{

}

///////////////////////////////////////

Lidar::Lidar(vector<Point2f> point2d, vector<Point3d> point3d, MatrixXf matrixImg, Mat imagen)
{
    point2D = point2d;
    point3D = point3d;
    matrixVeloToImg = matrixImg;
    imageFilePoints = imagen;
    fullMatrix=false;
    fullMatrixLabels=false;
}

///////////////////////////////////////

Mat Lidar::showCalibrationMatrix(Mat img, bool fullMatrixAux, QString pathLidar, QString pathCalib, QString auxNameFile, MatrixXf matrixVeloAux, bool lidarBoolean)
{
     fullMatrix = fullMatrixAux;
     matrixVeloToImg = matrixVeloAux;
     //HACER DOS MATRIX, UNO PARA LIDAR Y OTRO PARA 3D

     if(!fullMatrix || !fullMatrixLabels)
     {
        //Read calibration's file in .txt
        matrixVeloToImg = readCalibVelodyne(pathCalib.toStdString());
        matrixVeloToImg = readCalibCameraLeft(pathCalib.toStdString(), matrixVeloToImg);
        if(!fullMatrix)
            fullMatrix=true;
        if(!fullMatrixLabels)
            fullMatrixLabels=true;

     }

        img.copyTo(imageFilePoints);

        string pathLidarString = pathLidar.toStdString();
        pathLidarString = pathLidarString.append("/");
        pathLidarString = pathLidarString.append(auxNameFile.toStdString());
        pathLidarString = pathLidarString.append(".bin");

        //LEER NUBE CON LA FUNCION. LEE ARCHIVO BIN CORRESPONDIENTE A LA IMAGEN
        pcl::PointCloud<pcl::PointXYZI>::Ptr _cloud (getPointCloud(pathLidarString));

        pcl::PointCloud<pcl::PointXYZ>::Ptr xyz_cloud (new pcl::PointCloud<pcl::PointXYZ>);
        pcl::copyPointCloud(*_cloud,*xyz_cloud);

        //Obtener los puntos de la nube y poner el 4º valor a 1
        MatrixXf points_velodyne = xyz_cloud->getMatrixXfMap(3,4,0);
        points_velodyne.conservativeResize(4, NoChange_t());
       // cout << _cloud->width << ", " << points_velodyne.cols() << ", " << points_velodyne.rows() << endl;
        points_velodyne.row(3) =  MatrixXf::Constant(1, xyz_cloud->width, 1);

        //TRANSF_MATRIX SERÍA CAMARERA_PROJECTION, ALMACENAR TRANSF_MATRIX EN ALGUN MÉTODO PARA ASI NADA MAS ENTRAR A ESTE MÉTODO
        //LEER LA MATRIZ, SI ESTA VACIA SE HACE READ_CALIB, SINO NO SE LEE.
        //Y = P_rect_xx * R_rect_00 * (R|T)_velo_to_cam * X
        //pcl::transformPointCloud(*_cloud, *cam_cloud, pointVelodyne);
         MatrixXf point_velo2cam =  matrixVeloToImg * points_velodyne;

        //NORMALIZA PARA QUE QUEDE UNA MATRIZ DEL TIPO (U, V, 1)
        point_velo2cam.row(0) = point_velo2cam.row(0).cwiseQuotient(point_velo2cam.row(2));
        point_velo2cam.row(1) = point_velo2cam.row(1).cwiseQuotient(point_velo2cam.row(2));
        point_velo2cam.row(2) = MatrixXf::Constant(1, _cloud->width, 1);

        //Save in a vector de 2D points of the velodyne apearing on the image. Save also the correspondent 3D points
        for (int i = 0; i < point_velo2cam.cols(); i++)
        {
            if (points_velodyne(0,i) > 0 && point_velo2cam(0,i) >= 0 && point_velo2cam(1,i) >= 0  && point_velo2cam(0,i) < imageFilePoints.cols && point_velo2cam(1,i) < imageFilePoints.rows){
                cout<< "dentro "<< points_velodyne(0,i) <<endl;
                Point2f p (point_velo2cam(0,i), point_velo2cam(1,i));
                Point3d p_v (points_velodyne(0,i), points_velodyne(1,i), points_velodyne(2,i));
                int color = min(255, (int)(255*points_velodyne(0,i)/70.));    // 70 points_velodyne se pinta como lo mande a jorge
                //PINTA LOS PUNTOS EN LA IMAGEN
                circle(imageFilePoints, p, 1, Scalar(0, 0, color), -1);
                if(lidarBoolean){
                    point2DOriginal.push_back(p);
                    point3DOriginal.push_back(p_v);
                }
                else{
                    point2D.push_back(p);
                    point3D.push_back(p_v);
                }
            }
        }

        return imageFilePoints;
}

//////////////////////////////////
//Read pointscloud in .bin file
pcl::PointCloud<pcl::PointXYZI>::Ptr Lidar::getPointCloud(string path)
{
    pcl::PointCloud<pcl::PointXYZI>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZI>);
    std::fstream file(path.c_str(), ios::in | ios::binary);
    if(file.good()){
        file.seekg(0, ios::beg);
        int i;
        for (i = 0; file.good() && !file.eof(); i++) {
            pcl::PointXYZI point;
            file.read((char *) &point.x, 3*sizeof(float));
            file.read((char *) &point.intensity, sizeof(float));
            cloud->push_back(point);
        }
        file.close();
    }

    return cloud;
}

//////////////////////////////////

MatrixXf Lidar::readCalibVelodyne(string pathCalib)
{
    //Matrix velodyne
    MatrixXf velo2cam_rot_ (3,3);   //rotation matrix
    MatrixXf velo2cam_origin_ (1,3);    //traslation vector
    MatrixXf velo2cam_result(4,4);

    string pathCalibVeloToCam = pathCalib;
    pathCalibVeloToCam = pathCalibVeloToCam.append("/calib_velo_to_cam.txt");


    // Velodyne to cam
    FILE * v2c_calib_file = fopen(pathCalibVeloToCam.c_str(),"r");
    int line_count = 0;
    char str[255];

    // Skip first line
    fscanf(v2c_calib_file, "%*[^\n]\n", NULL);

    double R[9], T[3];

    fscanf(v2c_calib_file, "%s %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
                   str,
                   &R[0],    &R[1],    &R[2],
                   &R[3],    &R[4],    &R[5],
                   &R[6],    &R[7],    &R[8]);

    fscanf(v2c_calib_file, "%s %lf %lf %lf\n",
                   str,
                   &T[0],    &T[1],    &T[2]);

      velo2cam_result << R[0], R[1], R[2], T[0],
              R[3], R[4], R[5], T[1],
              R[6], R[7], R[8],T[2],
              0,    0,    0,   1;

      return velo2cam_result;
}

//////////////////////////////////
MatrixXf Lidar::readCalibCameraLeft(string pathCalib, MatrixXf velodyneResult)
{
    MatrixXf matrixCameraResult(3,4);
    MatrixXf P_Rect_2 (3,4);
    MatrixXf P_Rect_0 (3,4);
    MatrixXf R_Rect_00(4,4);
    MatrixXf T2 (4,4);


    string pathCalibCamToCam = pathCalib;
    pathCalibCamToCam = pathCalibCamToCam.append("/calib_cam_to_cam.txt");

    // Camera to camera calibration
    FILE * calib_file = fopen(pathCalibCamToCam.c_str(),"r");

    int line_count = 0;
    char str[255];

    for (line_count=1; line_count<9; line_count++){
        // Skip lines
        fscanf(calib_file, "%*[^\n]\n");
    }

    // All matrices are converted to 4x4
    double R_rect_00[16] = {0};
    fscanf(calib_file, "%s %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
                   str,
                   &R_rect_00[0],    &R_rect_00[1],    &R_rect_00[2],
                   &R_rect_00[4],    &R_rect_00[5],   &R_rect_00[6],
                   &R_rect_00[8],    &R_rect_00[9],    &R_rect_00[10]);
    R_rect_00[15] = 1;

    line_count++;

    double P_rect_0[12] = {0};
    fscanf(calib_file, "%s %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
                   str,
                   &P_rect_0[0],    &P_rect_0[1],    &P_rect_0[2],    &P_rect_0[3],
                   &P_rect_0[4],    &P_rect_0[5],    &P_rect_0[6],    &P_rect_0[7],
                   &P_rect_0[8],    &P_rect_0[9],    &P_rect_0[10],   &P_rect_0[11]);


    for (; line_count<25; line_count++){    //26
        // Skip lines
        fscanf(calib_file, "%*[^\n]\n", NULL);
    }

    double P_rect_2[12] = {0};
    fscanf(calib_file, "%s %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf\n",
                   str,
                   &P_rect_2[0],    &P_rect_2[1],    &P_rect_2[2],    &P_rect_2[3],
                   &P_rect_2[4],    &P_rect_2[5],    &P_rect_2[6],    &P_rect_2[7],
                   &P_rect_2[8],    &P_rect_2[9],    &P_rect_2[10],   &P_rect_2[11]);

    line_count++;

    for (; line_count<34; line_count++){
        fscanf(calib_file, "%*[^\n]\n", NULL);
    }

    P_Rect_2 << P_rect_2[0], P_rect_2[1], P_rect_2[2], P_rect_2[3],
                P_rect_2[4], P_rect_2[5], P_rect_2[6], P_rect_2[7],
                P_rect_2[8], P_rect_2[9], P_rect_2[10], P_rect_2[11];

    P_Rect_0 << P_rect_0[0], P_rect_0[1], P_rect_0[2], P_rect_0[3],
                P_rect_0[4], P_rect_0[5], P_rect_0[6], P_rect_0[7],
                P_rect_0[8], P_rect_0[9], P_rect_0[10], P_rect_0[11];

    R_Rect_00 <<  R_rect_00[0],    R_rect_00[1],    R_rect_00[2],   R_rect_00[3],
                  R_rect_00[4],    R_rect_00[5],    R_rect_00[6],   R_rect_00[7],
                  R_rect_00[8],    R_rect_00[9],    R_rect_00[10],  R_rect_00[11],
                  R_rect_00[12],   R_rect_00[13],   R_rect_00[14],  R_rect_00[15];

    //Create a unitary matrix
    double t2[16] = {0};
    t2[0] = t2[5] = t2[10] = t2[15] = 1;

    t2[3] = P_rect_2[3]/P_rect_2[0];

    T2 << t2[0], t2[1], t2[2], t2[3],
          t2[4], t2[5], t2[6], t2[7],
          t2[8], t2[9], t2[10], t2[11],
          t2[12], t2[13], t2[14], t2[15];

     //P_rect_xx * R_rect_00 * (R|T)_velo_to_cam
    matrixCameraResult = P_Rect_2 * R_Rect_00 * velodyneResult;

    return matrixCameraResult;
}

//////////////////////////////////

void Lidar::setMatrix(MatrixXf matrixVelodyne)
{
    matrixVeloToImg = matrixVelodyne;
}

//////////////////////////////////

vector<Point2f> Lidar::saveVector2d()
{
    return point2D;
}

//////////////////////////////////

vector<Point3d> Lidar::saveVector3d()
{
    return point3D;
}

//////////////////////////////////

Mat Lidar::getImage()
{
    return imageFilePoints;
}

//////////////////////////////////

bool Lidar::getFullMatrix()
{
    return fullMatrix;
}

//////////////////////////////////

void Lidar::setVectorLidar(vector<lidarinformation> lidarVector)
{
    my_lidarInformation2D = lidarVector;
}

//////////////////////////////////

/*vector<pcl::PointXYZRGB> Lidar::getVector3DLabel()
{
    return my_lidarInformation3D;
}*/

//////////////////////////////////

void Lidar::setFullMatrix(bool aux)
{
    fullMatrix=aux;
}

//////////////////////////////////

vector<lidarinformation> Lidar::getVectorLidarInformation()
{
    return my_lidarInformation2D;
}

//////////////////////////////////

MatrixXf Lidar::getMatrix()
{
    return matrixVeloToImg;
}

///////////////////////////////////////

void Lidar::setPoint3D(vector<Point3d> point3d)
{
    point3D = point3d;
}

///////////////////////////////////////

void Lidar::setPoint3DLabel(vector<pcl::PointXYZRGB> pointLabel)
{
    my_lidarInformation3D = pointLabel;
}

///////////////////////////////////////

void Lidar::setPoint2D(vector<Point2f> point2d)
{
    point2D = point2d;
}

///////////////////////////////////////
