#include "datasetdialog.h"
#include "ui_datasetdialog.h"

///////////////////////////////////////

DatasetDialog::DatasetDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DatasetDialog)
{
    ui->setupUi(this);
    selectedState=false;
}

///////////////////////////////////////

DatasetDialog::~DatasetDialog()
{
    delete ui;
}

///////////////////////////////////////

void DatasetDialog::on_selectionButton_clicked()
{
    selection[0] = ui->colorCB->isChecked();
    selection[1] = ui->idCB->isChecked();
    selection[2] = ui->instanciableCB->isChecked();
    selection[3] = ui->polygonesCB->isChecked();
    if(ui->currentFrameCB->isEnabled())
        selection[4] = ui->currentFrameCB->isChecked();

    selectedState=true;

    close();
}

///////////////////////////////////////

bool* DatasetDialog::getSelection()
{
    return selection;
}

///////////////////////////////////////

void DatasetDialog::enableCurrentFrame()
{
    ui->currentFrameCB->setEnabled(true);
}

///////////////////////////////////////

bool DatasetDialog::getSelectedState()
{
    return selectedState;
}

///////////////////////////////////////
