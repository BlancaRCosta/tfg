#include "instantiable.h"

///////////////////////////////////////

Instantiable::Instantiable()
{

}

///////////////////////////////////////

Instantiable::Instantiable(vector<Point> vertexes, QColor color, QString polygonName, int numPoly, bool instantiableState, int polygonID, QRectF BBox): Polygon(vertexes,color, polygonName, numPoly, instantiableState, polygonID, BBox)
{
    polyID=polygonID;

    Rect BoundingBox = boundingRect(polyVertexes);
    qreal x_BBox,  y_BBox, width_BBox, height_BBox;
    x_BBox= BoundingBox.x;
    y_BBox= BoundingBox.y;
    width_BBox=BoundingBox.width;
    height_BBox=BoundingBox.height;

    BBox.setRect(x_BBox,y_BBox,width_BBox,height_BBox);
    rectangle=BBox;

 }

///////////////////////////////////////

Instantiable::Instantiable(const Instantiable &Plg)
{
    polyVertexes=Plg.polyVertexes;
    polyColor=Plg.polyColor;
    polyName=Plg.polyName;
    polyID=Plg.polyID;
    instanStateP=Plg.instanStateP;
    rectangle=Plg.rectangle;

}

///////////////////////////////////////

Instantiable::Instantiable(Instantiable *instan)
{
    polyVertexes=instan->polyVertexes;
    polyColor=instan->polyColor;
    polyName=instan->polyName;
    polyID=instan->polyID;
    instanStateP=instan->instanStateP;
    rectangle=instan->rectangle;
}

///////////////////////////////////////

Instantiable::~Instantiable()
{

}

///////////////////////////////////////

void Instantiable::setPolyID(int id)
{
    polyID=id;
}

///////////////////////////////////////

int Instantiable::getPolyID()
{
    return polyID;
}

///////////////////////////////////////

void Instantiable::setBoundingBoxI()
{
    Rect BoundingBox = boundingRect(polyVertexes);
    qreal x_BBox,  y_BBox, width_BBox, height_BBox;
    x_BBox= BoundingBox.x;
    y_BBox= BoundingBox.y;
    width_BBox=BoundingBox.width;
    height_BBox=BoundingBox.height;

    rectangle.setRect(x_BBox,y_BBox,width_BBox,height_BBox);
}

///////////////////////////////////////

void Instantiable::setBoundingBoxInterpolation(QPointF topLeft, QPointF bottonRight)
{
    QRectF rectan(topLeft, bottonRight);
    rectangle=rectan;
}

///////////////////////////////////////

QRectF Instantiable::getBoundingBoxI()
{
    return rectangle;
}

///////////////////////////////////////

void Instantiable::modificatedBBoxI(double factor)
{
    QPointF topLeft = rectangle.topLeft();
    QPointF bottonRight =rectangle.bottomRight();

    topLeft = topLeft*factor;
    bottonRight = bottonRight*factor;

    QRectF rectan(topLeft, bottonRight);
    rectangle = rectan;
}

///////////////////////////////////////
