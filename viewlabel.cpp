#include "viewlabel.h"
#include "ui_viewlabel.h"

viewlabel::viewlabel(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::viewlabel)
{
    ui->setupUi(this);
}

viewlabel::~viewlabel()
{
    delete ui;
}

void viewlabel::setLabel(Label current_label)
{

    //Name
    QString currentLabel_name = current_label.getLabelName();
    ui->line_name->setText(currentLabel_name);
    ui->line_name->setReadOnly(true);

    //ID
    int currentLabel_ID = current_label.getLabelID();
    QString currentLabel_IDString = QString::number(currentLabel_ID,10);
    ui->line_id-> setText(currentLabel_IDString);
    ui->line_id->setReadOnly(true);

    //Color
    QColor currentLabel_color = current_label.getLabelColor();
    QPalette currentLabel_pal = ui->widgetColor->palette();
    currentLabel_pal.setColor(QPalette::Window, currentLabel_color);
    //colorWidget->setPalette(currentLabel_pal);
    ui->widgetColor->setPalette(currentLabel_pal);

    //Instanciable
    bool instac_state = current_label.getLabelInstantiableState();
    ui->line_instanciable->setReadOnly(true);
    if(instac_state==true)
        ui->line_instanciable->setText("Yes");
    else if(instac_state==false)
        ui->line_instanciable->setText("No");

    //Bounding Box
    bool boundingbox_state = current_label.getLabelBoundingBoxState();
    ui->line_boundingBox->setReadOnly(true);
    if(boundingbox_state==true)
        ui->line_boundingBox->setText("Yes");
    else if(boundingbox_state==false)
        ui->line_boundingBox->setText("No");

}
