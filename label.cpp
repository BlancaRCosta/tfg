#include "label.h"

///////////////////////////////////////

Label::Label()
{

}

///////////////////////////////////////

Label::Label(QString name, int id, QColor color, bool instanciable, bool boundingbox)
{
    label_name=name;
    label_id=id;
    label_color=color;
    label_instantiable=instanciable;
    label_boundingbox=boundingbox;
}

///////////////////////////////////////

Label::Label(const Label &Lbl)
{
    label_name=Lbl.label_name;
    label_id=Lbl.label_id;
    label_color=Lbl.label_color;
    label_instantiable=Lbl.label_instantiable;
    label_boundingbox=Lbl.label_boundingbox;

    my_Vertexes=Lbl.my_Vertexes;
    my_Polygones=Lbl.my_Polygones;
    my_PolygonesF=Lbl.my_PolygonesF;
    my_VertexesF=Lbl.my_VertexesF;
}

///////////////////////////////////////

Label::~Label()
{

}

///////////////////////////////////////

int Label::getLabelID()
{
    return label_id;
}

///////////////////////////////////////

QColor Label::getLabelColor()
{
    return label_color;
}

///////////////////////////////////////

QString Label::getLabelName()
{
    return label_name;
}

///////////////////////////////////////

bool Label::getLabelInstantiableState()
{
    return label_instantiable;
}

///////////////////////////////////////

bool Label::getLabelBoundingBoxState()
{
    return label_boundingbox;
}

///////////////////////////////////////

void Label::setLabelID(int new_ID)
{
    label_id=new_ID;
}

///////////////////////////////////////

void Label::setLabelColor(QColor new_color)
{
    label_color=new_color;
}

///////////////////////////////////////

void Label::setLabelName(QString new_name)
{
    label_name=new_name;
}

///////////////////////////////////////

void Label::setLabelInstantiableState(bool new_instanc_state)
{
    label_instantiable=new_instanc_state;
}

///////////////////////////////////////

void Label::setLabelBoundingBoxState(bool new_bbox_state)
{
    label_boundingbox=new_bbox_state;
}

///////////////////////////////////////

void Label::addVertex(Point newVertex)
{
    my_Vertexes.push_back(newVertex);
}
///////////////////////////////////////
void Label::addVertexF(QPointF newVertex)
{
    my_VertexesF.push_back(newVertex);
}

///////////////////////////////////////

void Label::deleteLastVertex()
{
    my_Vertexes.erase(my_Vertexes.end());
}

///////////////////////////////////////

void Label::deleteLastVertexF()
{
    my_VertexesF.erase(my_VertexesF.end());
}

///////////////////////////////////////

Point Label::getVertex(int index)
{
    return my_Vertexes[index];
}

///////////////////////////////////////

vector<Point> Label::getVertexes()
{
    return my_Vertexes;
}

///////////////////////////////////////

QVector<QPointF> Label::getVertexesF()
{
    return my_VertexesF;
}

///////////////////////////////////////

int Label::getNumVertexes()
{
    return my_Vertexes.size();
}

///////////////////////////////////////

void Label::setPolygonName(int index)
{
    my_Polygones[index]->setPolyName(index+1, label_name);
}

///////////////////////////////////////

void Label::setPolyID(int index, int id)
{
    Instantiable *instan = (Instantiable*) my_Polygones[index];
    instan->setPolyID(id);
}

///////////////////////////////////////

void Label::setNameFound(int index, QString name)
{
    my_Polygones[index]->setNameFound(name);
}

///////////////////////////////////////

void Label::setPolygonBBox(int index, QPointF topLeft, QPointF bottonRight)
{
    for(int i=0; i<my_Polygones.size(); i++)
    {
         Instantiable *instan = (Instantiable*) my_Polygones[index];
         instan->setBoundingBox(topLeft, bottonRight);
    }
}

///////////////////////////////////////

void Label::modificatedVertex(double factor)
{
    for(int i=0; i<my_Vertexes.size(); i++){
        my_Vertexes[i]=my_Vertexes[i]*factor;
    }
}

///////////////////////////////////////

void Label::modificatedVertexF(double factor)
{
    for(int i=0; i<my_VertexesF.size(); i++){
        my_VertexesF[i]=my_VertexesF[i]*factor;
    }
}

///////////////////////////////////////

void Label::modificatedPolygon(double factor)
{
    for(int i=0; i<my_Polygones.size(); i++){
        my_Polygones[i]->modificatedPolygon(factor);
    }
}

///////////////////////////////////////

void Label::modificatedBBoxP(double factor)
{
    for(int i=0; i<my_Polygones.size(); i++)
    {
        my_Polygones[i]->modificatedBBox(factor);
    }
}

///////////////////////////////////////
Instantiable* Label::createPolygonI(int polygonID, QRectF BoundingBox)
{
    int numPoly=my_Polygones.size()+1;
    Instantiable *newPolyInstan = new Instantiable (my_Vertexes, label_color, label_name, numPoly, label_instantiable, polygonID,BoundingBox);
    my_Polygones.push_back(newPolyInstan);

    my_Vertexes.clear();
    my_VertexesF.clear();

    return newPolyInstan;
}

///////////////////////////////////////
NoInstantiable* Label::createPolygonNO()
{
    int numPoly=my_Polygones.size()+1;
    NoInstantiable *newPolyNoInstan = new NoInstantiable(my_Vertexes, label_color, label_name, numPoly, label_instantiable, 0);
    my_Polygones.push_back(newPolyNoInstan);

    my_Vertexes.clear();
    my_VertexesF.clear();

    return newPolyNoInstan;

    }

///////////////////////////////////////

void Label::addPolygon(Polygon poly)
{
  //  my_Polygones.push_back(poly);
}

///////////////////////////////////////

Polygon *Label::getPolygon(int index)
{ 
    Polygon *poly = my_Polygones[index];
    return poly;
}

///////////////////////////////////////

QPolygonF Label::getPolygonF(int index)
{
    QPolygonF poly = my_PolygonesF[index];
    return poly;
}

///////////////////////////////////////

vector<Polygon*> Label::getPolygones()
{
    return my_Polygones;
}

///////////////////////////////////////

QVector<QPolygonF> Label::getPolygonesF()
{
    return my_PolygonesF;
}

///////////////////////////////////////

int Label::getNumPolygones()
{
    return my_Polygones.size();
}

///////////////////////////////////////

int Label::getNumPolygonesF()
{
    return my_PolygonesF.size();
}

///////////////////////////////////////

QString Label::getPolygonName(int index)
{
    return my_Polygones[index]->getPolyName();
}

///////////////////////////////////////

void Label::deletePolygon(int index)
{
    delete my_Polygones.at(index);
    my_Polygones.erase(my_Polygones.begin()+index);
}

///////////////////////////////////////

void Label::deletePolygonF(int index)
{
    my_PolygonesF.remove(index);
}

///////////////////////////////////////

void Label::clearPolygon(int index)
{
    my_Polygones.erase(my_Polygones.begin()+index);
}

///////////////////////////////////////

void Label::clearPolygonF(int index)
{
    my_PolygonesF.erase(my_PolygonesF.begin()+index);
}

///////////////////////////////////////

int Label::getPolyID(int index)
{
    Instantiable *instan = (Instantiable*) my_Polygones[index];
    return instan->getPolyID();
}

///////////////////////////////////////

void Label::setPolygon(Instantiable *poly)
{
     my_Polygones.push_back(poly);

}

///////////////////////////////////////

void Label::saveQPolygon(QPolygonF poly)
{
    my_PolygonesF.push_back(poly);
}

///////////////////////////////////////
