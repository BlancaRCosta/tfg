#include "noinstantiable.h"

///////////////////////////////////////

NoInstantiable::NoInstantiable()
{

}

///////////////////////////////////////

NoInstantiable::NoInstantiable(vector<Point> vertexes, QColor color, QString polygonName, int numPoly, bool instantiableState, int polygonID): Polygon(vertexes, color, polygonName, numPoly, instantiableState, polygonID, BBox)
{
    polyID=0;
}

///////////////////////////////////////

NoInstantiable::NoInstantiable(const NoInstantiable &Plg)
{
    polyVertexes=Plg.polyVertexes;
    polyColor=Plg.polyColor;
    polyName=Plg.polyName;
    instanStateP=Plg.instanStateP;
    polyID=Plg.polyID;
}

///////////////////////////////////////

int NoInstantiable::getPolyID()
{
    return polyID;
}

///////////////////////////////////////

void NoInstantiable::setPolyID(int id)
{
    polyID=id;
}

///////////////////////////////////////
