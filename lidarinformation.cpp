#include "lidarinformation.h"

lidarinformation::lidarinformation()
{

}

///////////////////////////////////////

void lidarinformation::setLabelID(int id)
{
    labelID = id;
}


///////////////////////////////////////

void lidarinformation::setLabelColor(QColor color)
{
    labelColor = color;
}

///////////////////////////////////////

void lidarinformation::setPoint(QPointF pointF)
{
    Point aux(pointF.x(), pointF.y());
    point=aux;
}

///////////////////////////////////////

int lidarinformation::getLabelID()
{
    return labelID;
}

///////////////////////////////////////

QColor lidarinformation::getLabelColor()
{
    return labelColor;
}

///////////////////////////////////////

Point lidarinformation::getPoint()
{
    return point;
}

///////////////////////////////////////
Point lidarinformation::getVertex(int index)
{
    Point point(my_VertexesF[index].x(), my_VertexesF[index].y());
    return point;
}


///////////////////////////////////////

int lidarinformation::getNumVertexes()
{
    return my_VertexesF.size();
}

///////////////////////////////////////
