# annotat3d
A tool to create image-pointcloud databases.

### How to build
```mkdir build```

```cd build/```

```cmake ..```

```make```

### Launch de app
From build/ folder:

```./annotat3d```
